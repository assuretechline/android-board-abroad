package com.boredabroad;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.boredabroad.adapter.AllTripAdapter;
import com.boredabroad.adapter.SideMenuAdapter;
import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.fragment.AddTripFragment;
import com.boredabroad.fragment.AllTripFragment;
import com.boredabroad.fragment.HomeFragment;
import com.boredabroad.fragment.MapFragment;
import com.boredabroad.fragment.PolicyTermFragment;
import com.boredabroad.fragment.SettingFragment;
import com.boredabroad.fragment.SocialFragment;
import com.boredabroad.global.GlobalApplication;
import com.boredabroad.global.MyPreference;
import com.boredabroad.global.Utility;
import com.boredabroad.model.AddImageListingData;
import com.boredabroad.model.MenuCategoryData;
import com.boredabroad.model.MenuData;
import com.boredabroad.model.TripListData;
import com.facebook.FacebookSdk;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;


public class MainActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    FrameLayout fl_main;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    ListView lstview_menu;
    String frg = "";
    ImageView imgv_menu;
    TextView txt_header, txt_menu_name, txt_menu_location;
    ProgressDialog pd;
    public static MainActivity mainActivity;
    SideMenuAdapter sideMenuAdapter;
    ArrayList<MenuData> slideMenuArrayList = new ArrayList<>();
    APIServer apiServer;

    public static MainActivity getInstance() {
        return mainActivity;
    }

    String HOME = "Home", MY_TRIPS = "My Tripes", UPDATE_ACTIVITIES = "Update Activites", SOCIAL_CONNECTION = "Social Connections", ADD_NEW_TRIP = "Add New Trip", HOW_IT_WORKS = "How It Works", TERM_OF_USE = "Terms Of Use", PRIVACY_POLICY = "Privacy Policy", SETTING = "Settings", LOGOUT = "Logout";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        setView();
        setData();
        setListener();
        setColor();
    }

    private void initialize() {
        context = this;
        mainActivity = this;
        apiServer = new APIServer(context);
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        FacebookSdk.sdkInitialize(context);

        try {
            ProviderInstaller.installIfNeeded(this);
        } catch (GooglePlayServicesRepairableException e) {
            // Thrown when Google Play Services is not installed, up-to-date, or enabled
            // Show dialog to allow users to install, update, or otherwise enable Google Play services.
            GooglePlayServicesUtil.getErrorDialog(e.getConnectionStatusCode(), this, 0);
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("SecurityException", "Google Play Services not available.");
        }
    }

    private void setView() {
        fl_main = findViewById(R.id.fl_main);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navigationView);
        lstview_menu = findViewById(R.id.lstview_menu);
        imgv_menu = findViewById(R.id.imgv_menu);
        txt_header = (TextView) findViewById(R.id.txt_header);
        txt_menu_name = (TextView) findViewById(R.id.txt_menu_name);
        txt_menu_location = (TextView) findViewById(R.id.txt_menu_location);
    }

    private void setData() {
        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e("Tag id login", "Refreshed token: " + refreshedToken);
            addDeviceInfo(refreshedToken);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            txt_menu_name.setText(MyPreference.getPreferenceValue(context, "name").toString());
            txt_menu_location.setText(MyPreference.getPreferenceValue(context, "email").toString());
        } catch (Exception e) {

        }
        if (Utility.isNetworkAvailable(context)) {
            getTrip();
        } else {
            Utility.errDialog(context.getString(R.string.network), context);
        }

        slideMenuArrayList.add(new MenuData(HOME, R.drawable.home, true));
        slideMenuArrayList.add(new MenuData(MY_TRIPS, R.drawable.notes, false));
        //slideMenuArrayList.add(new MenuData(UPDATE_ACTIVITIES, R.drawable.update_activity, false));
        slideMenuArrayList.add(new MenuData(SOCIAL_CONNECTION, R.drawable.history, false));
        slideMenuArrayList.add(new MenuData(ADD_NEW_TRIP, R.drawable.placeholder, false));
        slideMenuArrayList.add(new MenuData(HOW_IT_WORKS, R.drawable.info, false));
        slideMenuArrayList.add(new MenuData(TERM_OF_USE, R.drawable.info, false));
        slideMenuArrayList.add(new MenuData(PRIVACY_POLICY, R.drawable.info, false));
        slideMenuArrayList.add(new MenuData(SETTING, R.drawable.setting, false));
        slideMenuArrayList.add(new MenuData(LOGOUT, R.drawable.logout, false));

        sideMenuAdapter = new SideMenuAdapter(context, slideMenuArrayList);
        lstview_menu.setAdapter(sideMenuAdapter);



        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private void firstFragmentLoad(boolean isMap) {
        if (isMap) {
            setFragmentMainTab(new MapFragment(), "MapFragment");
        } else {
            setFragmentMainTab(new MapFragment(), "MapFragment");
        }
    }

    private void setListener() {
        imgv_menu.setOnClickListener(this);
        lstview_menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                handleMenuClick(i);
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
        });
    }

    public void handleMenuClick(int i) {
        for (int j = 0; j < slideMenuArrayList.size(); j++) {
            slideMenuArrayList.get(j).setSelected(false);
        }
        slideMenuArrayList.get(i).setSelected(true);
        sideMenuAdapter.notifyDataSetChanged();

        if (slideMenuArrayList.get(i).getTitle().equals(HOME)) {
            setFragmentMainTab(new MapFragment(), "MapFragment");
        } else if (slideMenuArrayList.get(i).getTitle().equals(MY_TRIPS)) {
            setFragmentMainTab(new AllTripFragment(), "AllTripFragment");
        } else if (slideMenuArrayList.get(i).getTitle().equals(UPDATE_ACTIVITIES)) {
            setFragmentMainTab(new AddTripFragment(true), "AddTripFragment");
        } else if (slideMenuArrayList.get(i).getTitle().equals(SOCIAL_CONNECTION)) {
            setFragmentMainTab(new SocialFragment(false), "SocialActivity");
        } else if (slideMenuArrayList.get(i).getTitle().equals(ADD_NEW_TRIP)) {
            setFragmentMainTab(new AddTripFragment(false), "AddTripFragment2");
        } else if (slideMenuArrayList.get(i).getTitle().equals(SETTING)) {
            setFragmentMainTab(new SettingFragment(), "SettingFragment");
        } else if (slideMenuArrayList.get(i).getTitle().equals(LOGOUT)) {
            setLogout();
        } else if (slideMenuArrayList.get(i).getTitle().equals(PRIVACY_POLICY)) {
            setFragmentMainTab(new PolicyTermFragment("policy"), "PolicyTermFragment1");
        } else if (slideMenuArrayList.get(i).getTitle().equals(HOW_IT_WORKS)) {
            setFragmentMainTab(new PolicyTermFragment("how"), "PolicyTermFragment2");
        } else if (slideMenuArrayList.get(i).getTitle().equals(TERM_OF_USE)) {
            setFragmentMainTab(new PolicyTermFragment("term"), "PolicyTermFragment3");
        }

    }

    private void setColor() {
        imgv_menu.setColorFilter(context.getResources().getColor(R.color.font_saff));
    }

    @Override
    public void onBackPressed() {
        setBackPressed();
    }

    public void setBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            finish();
            System.exit(0);
        }
    }

    public void setDrawer() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_menu) {
            setDrawer();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_main, fragment, tag).addToBackStack(tag).commit();
    }

    public void setFragmentMainTab(Fragment fragment, String tag) {
        if (tag.equals(frg)) {
        } else {
            frg = tag;
            getSupportFragmentManager().beginTransaction().replace(fl_main.getId(), fragment, tag).addToBackStack(tag).commitAllowingStateLoss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void setLogout() {
        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
        alertDialog2.setTitle("Logout");
        alertDialog2.setMessage("Are you sure you want to Logout?");
        alertDialog2.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        MyPreference.setPreferenceValue(context, "isLogin", "false");
                        MyPreference.setPreferenceValue(context, "mainData", "");
                        Intent i = new Intent(context, LoginActivity.class);
                        context.startActivity(i);
                        ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                        finish();
                    }
                });

        alertDialog2.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog2.show();
    }

    private void addDeviceInfo(String refreshedToken) {
        Log.e("Tag", "refreshedToken addDeviceInfo : " + refreshedToken);
        //pd=Utility.showProgressDialog(context);
        apiServer.addDevice(new APIResponse() {
                                @Override
                                public void onSuccess(JSONObject object) {
                                    Log.e("Tag", "data  addDeviceInfo : " + object);
                                    try {
                                        Log.e("Tag", "data  addDeviceInfo : " + object);
                                        if (object.getString("type").equals("success")) {
                                            //Utility.dismissProgressDialog(pd);
                                        } else {
                                            //Utility.dismissProgressDialog(pd);
                                            //Utility.errDialog( object.getString("message"),context);
                                        }
                                    } catch (JSONException e) {
                                        Utility.dismissProgressDialog(pd);
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(String error) {
                                    //Log.e("Tag","onFailure  changes addDevice : ");
                                    //Utility.dismissProgressDialog(pd);
                                    Log.e("Tag", "onFailure addDeviceInfo : " + error);
                                    //Utility.errDialog(context.getString(R.string.api_failure), context);
                                }
                            }, MyPreference.getPreferenceValue(context, "id").toString(),
                "Android",
                Utility.getVersionName() + "",
                Utility.getDeviceName() + "",
                Utility.getOSVersion() + "",
                Utility.getDeviceId(context) + "",
                refreshedToken + "");
    }

    private void getTrip() {
        pd = Utility.showProgressDialog(context);
        apiServer.getUserDetail(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    //Log.e("Tag","data changes : "+object);
                    if (object.getString("type").equals("success")) {
                        Utility.dismissProgressDialog(pd);
                        JSONArray jsonArray = object.getJSONObject("data").getJSONArray("trip");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            if (jsonObject.getString("date").length() > 7) {
                                //Log.e("Tag", "jsonObject : " + jsonObject.getString("date"));
                                Utility.dismissProgressDialog(pd);
                                if (Utility.compareDate(jsonObject.getString("date")).equals("0")) {
                                    firstFragmentLoad(true);
                                } else {
                                    firstFragmentLoad(false);
                                }
                                break;
                            }
                        }
                    } else {
                        Utility.dismissProgressDialog(pd);
                        firstFragmentLoad(true);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                    firstFragmentLoad(true);
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                firstFragmentLoad(true);
            }
        }, MyPreference.getPreferenceValue(context, "id").toString());
    }

}

