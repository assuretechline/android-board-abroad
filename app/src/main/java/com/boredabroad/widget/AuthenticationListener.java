package com.boredabroad.widget;

public interface  AuthenticationListener {
    public void onTokenReceived(String auth_token);
}
