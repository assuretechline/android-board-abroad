package com.boredabroad;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.boredabroad.adapter.AutoCompleteAdapter;
import com.boredabroad.fragment.AddTripFragment;
import com.boredabroad.global.Utility;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


public class SearchLocationActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    AutoCompleteTextView autoCompleteTextView;
    AutoCompleteAdapter adapter;
    TextView responseView,txt_cancel;
    PlacesClient placesClient;
    String apiKey;
    Geocoder mGeocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);
        initialize();
        setView();
        setData();
        setListener();
        setColor();
    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
    }

    private void setView() {
        responseView = findViewById(R.id.response);
        txt_cancel = findViewById(R.id.txt_cancel);
    }

    private void setData() {
        mGeocoder = new Geocoder(context, Locale.getDefault());
        apiKey = getString(R.string.google_map_key);
        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        placesClient = Places.createClient(this);
        initAutoCompleteTextView();
    }

    private void setListener() {
        txt_cancel.setOnClickListener(this);
    }

    private void setColor() {

    }

    @Override
    public void onBackPressed() {
        setBackPressed();
    }

    public void setBackPressed() {
        Utility.gotoBack(context);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initAutoCompleteTextView() {
        autoCompleteTextView = findViewById(R.id.auto);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setOnItemClickListener(autocompleteClickListener);
        adapter = new AutoCompleteAdapter(context, placesClient);
        autoCompleteTextView.setAdapter(adapter);
    }

    private AdapterView.OnItemClickListener autocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            try {
                final AutocompletePrediction item = adapter.getItem(i);
                String placeID = null;
                if (item != null) {
                    placeID = item.getPlaceId();
                }

                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

                FetchPlaceRequest request = null;
                if (placeID != null) {
                    request = FetchPlaceRequest.builder(placeID, placeFields).build();
                }

                if (request != null) {
                    placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onSuccess(FetchPlaceResponse task) {
                            //responseView.setText(task.getPlace().getName() + "\n" + task.getPlace().getAddress());
                            AddTripFragment.txt_location.setText(task.getPlace().getName());
                            AddTripFragment.lat=task.getPlace().getLatLng().latitude+"";
                            AddTripFragment.lang=task.getPlace().getLatLng().longitude+"";
                            //AddTripFragment.city=task.getPlace().getAddress();
                            try {
                                getPlaceInfo(task.getPlace().getLatLng().latitude,task.getPlace().getLatLng().longitude);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                            //responseView.setText(e.getMessage());
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void getPlaceInfo(double lat, double lon) throws IOException {
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);

        if (addresses.get(0).getLocality() != null) {
            String city = addresses.get(0).getLocality();
            AddTripFragment.city=city;
            Log.e("CITY",city);
        }

        if (addresses.get(0).getAdminArea() != null) {
            String state = addresses.get(0).getAdminArea();
            AddTripFragment.state=state;
            Log.e("STATE",state);
        }

        if (addresses.get(0).getCountryName() != null) {
            String country = addresses.get(0).getCountryName();
            AddTripFragment.country=country;
            Log.e("COUNTRY",country);
        }

        Utility.gotoBack(context);
    }

    @Override
    public void onClick(View v) {
        if(v==txt_cancel){
            autoCompleteTextView.setText("");
        }
    }
}

