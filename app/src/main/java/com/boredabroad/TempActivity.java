package com.boredabroad;

import android.content.Context;
import android.os.Bundle;
import android.view.View;


/**
 * Created by home on 12/07/18.
 */

public class TempActivity extends BaseActivity implements View.OnClickListener {
    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        setView();
        setData();
        setListener();
        setColor();

    }

    private void initialize() {
        context=this;
    }

    private void setView() {

    }

    private void setData() {

    }

    private void setListener() {

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
