package com.boredabroad.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.boredabroad.ProfileActivity;
import com.boredabroad.R;
import com.boredabroad.global.GlobalApplication;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Assure on 06-08-2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Tag";

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "****From: " + remoteMessage.getFrom());
        Log.e(TAG, "****isAppIsInBackground: " + NotificationUtils.isAppIsInBackground(getApplicationContext()));


        //Toast.makeText(getApplicationContext(),remoteMessage.getFrom(),Toast.LENGTH_LONG).show();

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //Log.e(TAG, "****Notification Body: " + remoteMessage.getNotification().getBody());
            //Log.e(TAG, "****Notification Body size : " + remoteMessage.getData().size());
            GlobalApplication.NotiData=remoteMessage.getFrom()+" : "+ remoteMessage.getNotification().getBody();

            if (remoteMessage.getData().size() > 0) {
                try {
                    JSONObject json = new JSONObject(remoteMessage.getData().toString());
                    Log.e("Tag", "****Data Payload json : " + remoteMessage.getData().toString());
                    handleNotification(remoteMessage.getNotification().getBody(), json);
                } catch (Exception e) {
                    //Log.e(TAG, "****Exception: " + e.getMessage());
                }
            } else {
                //Log.e(TAG, "****Exception size ");
            }
        }
    }

    private void handleNotification(String message, JSONObject json) {
        //Log.e("Tag", "is in back = " + NotificationUtils.isAppIsInBackground(getApplicationContext()));
        try {
            Log.e("Tag", "noticode = " + json.getInt("noticode") );
        } catch (JSONException e) {
            e.printStackTrace();
            //Log.e("Tag","Exception 4 : "+e.getMessage());
        }
        Intent resultIntent = null; //   default
        String title = "Bored Abroad";
        try {
            GlobalApplication.NotiData=json.toString();
            //Log.e("Tag","Notification Data : "+json.toString());
            if (json.getInt("noticode") == 1) {  //ProfileActivity
                resultIntent = new Intent(getApplicationContext(), ProfileActivity.class);
                resultIntent.putExtra("notiData",json.getJSONObject("data")+"");
                if(!NotificationUtils.isAppIsInBackground(getApplicationContext())){
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);
                    String channelId = "Default";
                    NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle(title)
                            .setContentText(message).setAutoCancel(true).setContentIntent(pendingIntent);;
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
                        manager.createNotificationChannel(channel);
                    }
                    int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                    manager.notify(m, builder.build());
                }
            }

            resultIntent.putExtra("message", message);
        } catch (Exception e) {
            //Log.e("Tag","Exception 5 : "+e.getMessage());
            e.printStackTrace();
        }

/*        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
        final PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);

        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/raw/notification");
        Notification notification = mBuilder
                .setTicker(title)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setSmallIcon(R.drawable.logo)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.logo))
                .setContentText(message)
                .build();
        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(m, notification);*/
    }


    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
        notificationUtils.playNotificationSound();
    }

}
