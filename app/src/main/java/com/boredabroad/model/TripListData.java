package com.boredabroad.model;

public class TripListData {
    String name,date,activity,location;

    public TripListData(String name, String date, String activity, String location) {
        this.name = name;
        this.date = date;
        this.activity = activity;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
