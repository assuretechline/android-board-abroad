package com.boredabroad.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class User implements ClusterItem {
    private final String username;
    private final LatLng latLng;
    private final String url;
    public User(String username, LatLng latLng,String url) {
        this.username = username;
        this.latLng = latLng;
        this.url = url;
    }
    @Override
    public LatLng getPosition() {  // 1
        return latLng;
    }
    @Override
    public String getTitle() {  // 2
        return username;
    }
    @Override
    public String getSnippet() {
        return "";
    }

    public String getUrl() {
        return url;
    }
}