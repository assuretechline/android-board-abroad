package com.boredabroad.model;

/**
 * Created by Assure on 02-07-2018.
 */

public class MenuCategoryData {
    String id,cat_name,cat_image;
    boolean isSelected;

    public MenuCategoryData(String id, String cat_name, String cat_image, boolean isSelected) {
        this.id = id;
        this.cat_name = cat_name;
        this.cat_image = cat_image;
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_image() {
        return cat_image;
    }

    public void setCat_image(String cat_image) {
        this.cat_image = cat_image;
    }
}
