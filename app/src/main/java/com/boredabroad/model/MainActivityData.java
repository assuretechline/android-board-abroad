package com.boredabroad.model;

import java.util.ArrayList;

public class MainActivityData {
    String id,name;
    ArrayList<SubActivityData> subActivityDataArrayList;

    public MainActivityData(String id,String name, ArrayList<SubActivityData> subActivityDataArrayList) {
        this.id = id;
        this.name = name;
        this.subActivityDataArrayList = subActivityDataArrayList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SubActivityData> getSubActivityDataArrayList() {
        return subActivityDataArrayList;
    }

    public void setSubActivityDataArrayList(ArrayList<SubActivityData> subActivityDataArrayList) {
        this.subActivityDataArrayList = subActivityDataArrayList;
    }
}
