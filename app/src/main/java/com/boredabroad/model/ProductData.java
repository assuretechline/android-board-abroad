package com.boredabroad.model;

/**
 * Created by Amisha on 21-Feb-18.
 */

public class ProductData {
    String name, image;
    int  price,available, userRequire,total;
    boolean isFavorite;

    public ProductData(String name, String image, int price, int available, int userRequire, int total, boolean isFavorite) {
        this.name = name;
        this.price = price;
        this.available = available;
        this.total = total;
        this.image = image;
        this.userRequire = userRequire;
        this.isFavorite = isFavorite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public int getUserRequire() {
        return userRequire;
    }

    public void setUserRequire(int userRequire) {
        this.userRequire = userRequire;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
