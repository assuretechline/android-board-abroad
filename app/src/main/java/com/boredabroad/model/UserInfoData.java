package com.boredabroad.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.io.Serializable;
import java.util.ArrayList;

public class UserInfoData implements Serializable, ClusterItem {
    String id,name, location, activity, image;
    double lat, lang;
    ArrayList<String> imageList;
    private  LatLng mPosition;
    String social;

    public UserInfoData(String name, String location, String activity, double lat, double lang, ArrayList<String> imageList) {
        this.name = name;
        this.location = location;
        this.activity = activity;
        this.lat = lat;
        this.lang = lang;
        this.imageList = imageList;
        mPosition = new LatLng(lat, lang);
    }

    public UserInfoData(String id, String name, String location, String activity, double lat, double lang, String image, String social) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.activity = activity;
        this.lat = lat;
        this.lang = lang;
        this.image = image;
        this.social = social;
        mPosition = new LatLng(lat, lang);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLang() {
        return lang;
    }

    public void setLang(double lang) {
        this.lang = lang;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<String> imageList) {
        this.imageList = imageList;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }
}
