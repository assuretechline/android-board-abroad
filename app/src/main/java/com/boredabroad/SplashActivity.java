package com.boredabroad;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.boredabroad.global.MyPreference;
import com.boredabroad.global.Utility;

import org.json.JSONException;
import org.json.JSONObject;


public class SplashActivity extends BaseActivity implements View.OnClickListener {
    private static int SPLASH_TIME_OUT = 2000;
    Context context;
    ImageView imgv_logo;
    int noticode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();
    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
    }

    private void setView() {
        imgv_logo = findViewById(R.id.imgv_logo);
    }

    private void setData() {
        boolean isRedirect=false;
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                //Log.e("Tag Splash : ", "Key: " + key + " Value: " + value);
                if (key.equalsIgnoreCase("noticode")) {
                    noticode = Integer.parseInt(value + "");
                    if(noticode==1){
                        try {
                            //Log.e("Tag","Data test noti : "+new JSONObject(getIntent().getExtras().get("data").toString()));
                            isRedirect=true;
                            Intent i = new Intent(context, ProfileActivity.class);
                            i.putExtra("notiData",getIntent().getExtras().get("data").toString());
                            context.startActivity(i);
                            ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                            finish();
                        } catch (Exception e) {
                            isRedirect=true;
                            redirectNormal();
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
            if(isRedirect==false){
                redirectNormal();
            }
        }else {
            redirectNormal();
        }
    }

    private void redirectNormal(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Utility.gotoNext(context, SocialActivity.class);
                if (MyPreference.getPreferenceValue(context, "isLogin") != null) {
                    if (MyPreference.getPreferenceValue(context, "isLogin").equals("true")) {
                        Utility.gotoNext(context, MainActivity.class);
                    } else {
                        Utility.gotoNext(context, LoginActivity.class);
                    }
                } else {
                    Utility.gotoNext(context, LoginActivity.class);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void setLitionar() {

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
