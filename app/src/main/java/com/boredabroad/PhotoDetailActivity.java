package com.boredabroad;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.View;
import android.widget.ImageView;

import androidx.viewpager.widget.ViewPager;

import com.boredabroad.adapter.GalleryPagerAdapter;
import com.boredabroad.global.Utility;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by home on 19/07/18.
 */

public class PhotoDetailActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    ImageView imgv_back;
    public static ImageView imgv_blur;
    ViewPager vpgr_img;
    GalleryPagerAdapter galleryPagerAdapter;
    int position_m = 0;
    ImageLoader imageLoader;
    JSONArray imageList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_detail);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();
        Utility.checkAndRequestPermissions(context);
    }

    private void initialize() {
        context = this;
        Utility.crashLytics(context);
        Utility.setStatusColor(this);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        try {
            imageList=new JSONArray(getIntent().getStringExtra("data"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        galleryPagerAdapter = new GalleryPagerAdapter(getSupportFragmentManager(),imageList);
    }

    private void setView() {
        imgv_back = findViewById(R.id.imgv_back);
        vpgr_img = findViewById(R.id.vpgr_img);
        imgv_blur = findViewById(R.id.imgv_blur);
    }

    private void setData() {
        vpgr_img.setOffscreenPageLimit(1);
        vpgr_img.setAdapter(galleryPagerAdapter);
        setBlurImageData(0);
    }

    private void setLitionar() {
        imgv_back.setOnClickListener(this);

        vpgr_img.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setBlurImageData(position);
                position_m = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.saffron));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.gotoBack(context);
    }

    private void setBlurImageData(int position) {

        try {
            imageLoader.displayImage(imageList.getJSONObject(position).getString("thumb"), imgv_blur, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    imgv_blur.setImageBitmap(blurRenderScript(loadedImage, 25));
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Bitmap blurRenderScript(Bitmap smallBitmap, int radius) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

}
