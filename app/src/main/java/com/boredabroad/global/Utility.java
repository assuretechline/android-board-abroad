package com.boredabroad.global;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.boredabroad.BuildConfig;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.boredabroad.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by acer on 08-05-2017.
 */
public class Utility {

    static int w20, w50, w40, w10, w15, w8, w5, w70, w120, w250, w290, h3;
    static int h20, h2, h10, h15, h8, h7, h5, h290;

    public static Calendar cal;
    public static Date date;

    public static DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static SimpleDateFormat inputFormate1 = new SimpleDateFormat("dd-MM-yyyy");

    public static SimpleDateFormat serverDate = new SimpleDateFormat("yyyy-MM-dd");

    public static SimpleDateFormat inputFormate2 = new SimpleDateFormat("dd/MM/yyyy");

    public static DateFormat calenderFormat = new SimpleDateFormat("yyyy-MM-d HH:mm:ss");

    public static SimpleDateFormat outputFormate1 = new SimpleDateFormat("dd MMM yyyy");

    public static SimpleDateFormat onlyDateFormate = new SimpleDateFormat("dd");

    public static SimpleDateFormat onlyMonthYearFormate = new SimpleDateFormat("MMM ''yy");

    public static byte[] data;

    public static void calculate(int width, int height) {
        w8 = (int) width * 8 / 320;
        w10 = (int) width * 10 / 320;
        w5 = (int) width * 5 / 320;
        w15 = (int) width * 15 / 320;
        w50 = (int) width * 50 / 320;
        w40 = (int) width * 40 / 320;
        w20 = (int) width * 20 / 320;
        w120 = (int) width * 120 / 320;
        w70 = (int) width * 70 / 320;
        w250 = (int) width * 250 / 320;
        w290 = (int) width * 290 / 320;

        h10 = (int) height * 10 / 480;
        h2 = (int) height * 2 / 480;
        h8 = (int) height * 8 / 480;
        h3 = (int) height * 3 / 480;
        h5 = (int) height * 5 / 480;
        h7 = (int) height * 7 / 480;
        h15 = (int) height * 15 / 480;
        h20 = (int) height * 20 / 480;
        h290 = (int) height * 290 / 480;
    }


    public static String getDecodeString(String encodeString) {
        try {
            data = Base64.decode(encodeString, Base64.DEFAULT);
            encodeString = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodeString;
    }

    public static String encodeString(String encodeString) {
        @SuppressLint({"NewApi", "LocalSuppress"}) byte[] data = encodeString.getBytes(StandardCharsets.UTF_8);
        return Base64.encodeToString(data, Base64.DEFAULT);
    }


    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getOSVersion() {
        return android.os.Build.VERSION.SDK_INT + "";
    }

    public static String getVersionName() {
        return BuildConfig.VERSION_NAME;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static File compressImage(String path, Context context, String name) {
        Bitmap bmp = BitmapFactory.decodeFile(path);
        if (bmp.getHeight() > 150 && bmp.getWidth() > 150) {
            File filesDir = context.getFilesDir();
            File imageFile = new File(filesDir, name);
            OutputStream os;
            try {
                os = new FileOutputStream(imageFile);
                bmp.compress(Bitmap.CompressFormat.JPEG, 50, os);
                os.flush();
                os.close();
            } catch (Exception e) {

            }

            return imageFile;
        } else
            return new File(path);
    }

    public static void errDialog(String error, Context context) {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog2.setTitle("Alert");

        // Setting Dialog Message
        alertDialog2.setMessage(error);

        alertDialog2.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        alertDialog2.show();
    }

    public static void successDialog(String error, Context context) {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog2.setTitle("Connected");

        // Setting Dialog Message
        alertDialog2.setMessage(error);

        alertDialog2.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        alertDialog2.show();
    }

    public static void crashLytics(Context context) {
        if (Build.VERSION.SDK_INT > 19) {
            //Fabric.with(context, new Crashlytics());
        }
    }

    public static void setGridViewHeightBasedOnChildren(GridView gridView,
                                                        int columncount) {
        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        int size = gridView.getAdapter().getCount();
        int dynamicHeight = getTotalHeightofListView(gridView);
        if (size % columncount == 0) {
            params.height = (dynamicHeight / columncount) + 25;
        } else {
            params.height = (dynamicHeight / columncount)
                    + (dynamicHeight / size);
        }
        gridView.setLayoutParams(params);
        gridView.requestLayout();
    }

    public static int getTotalHeightofListView(AdapterView argAdapterView) {
        Adapter mAdapter = argAdapterView.getAdapter();
        if (mAdapter == null) {
            // pre-condition
            return 0;
        }
        int totalHeight = 0;
        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, argAdapterView);
            mView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            totalHeight += mView.getMeasuredHeight();
        }
        return totalHeight;
    }

    public static void gotoNext(Context context, Class aClass) {
        Intent i = new Intent(context, aClass);
        context.startActivity(i);
        ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    public static void gotoBack(Context context) {
        ((AppCompatActivity) context).finish();
        ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }

    public static String getUserCountry(Context context) {
        try {
            TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            return  tm.getNetworkCountryIso();
        } catch (Exception e) {
        }
        return null;
    }

    public static double getLattitude(Context context) {
        //permissionForLocation(context);
        GPSTracker gps = new GPSTracker(context);
        double latitude = 0.0;
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
        } else {
            gps.showSettingsAlert();
        }
        return latitude;
    }

    public static double getLongitude(Context context) {
        // permissionForLocation(context);
        GPSTracker gps = new GPSTracker(context);
        double longitude = 0.0;
        if (gps.canGetLocation()) {
            longitude = gps.getLongitude();
        } else {
            //gps.showSettingsAlert();
        }
        return longitude;
    }

    public static void permissionForLocation(final Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions((Activity) context, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    GlobalApplication.RESULT_LOCATION);
        }
    }

    public static void setStatusColor(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.theme));
        }
    }

    public static void setLoginStatusColor(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.theme));
        }
    }

    public static boolean checkLocationPermission(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public static void requestLocationPermission(Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
        } else {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GlobalApplication.RESULT_LOCATION);
        }
    }

    public static void expand(final View v, int txt_height) {
        try {
            v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final int targetHeight = txt_height;
            v.getLayoutParams().height = 1;
            v.setVisibility(View.VISIBLE);
            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime,
                                                   Transformation t) {
                    int temp = (interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime));
                    if (temp > 0) {
                        v.getLayoutParams().height = temp;
                    }
                    v.requestLayout();
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            a.setDuration(300);
            v.startAnimation(a);
        } catch (Exception e) {
            //  Log.d("tag->exception",e.toString());
        }
    }


    public static void collapse(final View v) {
        try {
            final int initialHeight = v.getMeasuredHeight();
            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime,
                                                   Transformation t) {
                    if (interpolatedTime == 1) {
                        v.setVisibility(View.GONE);
                    } else {
                        v.getLayoutParams().height = initialHeight
                                - (int) (initialHeight * interpolatedTime);
                        v.requestLayout();
                    }
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };
            a.setDuration(300);
            v.startAnimation(a);
        } catch (Exception e) {
        }
    }

    public static int getViewHeight(View view) {
        WindowManager wm =
                (WindowManager) view.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int deviceWidth;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            display.getSize(size);
            deviceWidth = size.x;
        } else {
            deviceWidth = display.getWidth();
        }

        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(widthMeasureSpec, heightMeasureSpec);
        return view.getMeasuredHeight(); //        view.getMeasuredWidth();
    }

    public static void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public static void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public static String getAppPath() {
        File file = new File(Environment.getExternalStorageDirectory() + "/Riddhi Doors");
        if (!file.exists()) {
            file.mkdirs();
        }
        return Environment.getExternalStorageDirectory() + "/Riddhi Doors";
    }

    public static String getFileNameFromUrl(String str) {
        return str.substring(str.lastIndexOf('/') + 1, str.length());

    }

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }


    public static DisplayImageOptions getImageOptions() {
        return new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.no_image)
                .showImageOnLoading(R.drawable.no_image)
                .showImageOnFail(R.drawable.no_image)
                .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    public static DisplayImageOptions getUserProfileOption() {
        return new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.no_profile)
                .showImageOnLoading(R.drawable.no_profile)
                .showImageOnFail(R.drawable.no_profile)
                .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }


    public static void hideSoftKeyboard(View view, Context context) {

        try {
            InputMethodManager inputManager = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            if (view != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCurrentDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    public static String getCurrentDateF1() {
        return new SimpleDateFormat("dd MMM, yyyy").format(Calendar.getInstance().getTime());
    }

    public static String getCurrentTime() {
        return new SimpleDateFormat("hh:mm a").format(Calendar.getInstance().getTime());
    }

    public static String getCurrentYear() {
        return new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime());
    }

    public static String getCurrentTimestamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();
    }

    public static boolean verifyStoragePermissions(Context context) {
        // Check if we have write permission
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkAndRequestPermissions(Context context) {

        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();


        if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((AppCompatActivity) context,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    1005);
            return false;
        }
        return true;
    }

    public static ProgressDialog showProgressDialog(Context context) {
        ProgressDialog progressDoalog = ProgressDialog.show(context, null, null);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.setCancelable(false);
        ProgressBar progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyle);
        //progressBar.getProgressDrawable().setColorFilter(context.getResources().getColor(R.color.line_blue), PorterDuff.Mode.SRC_IN);
        //progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.app_background));
        progressBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.saffron), PorterDuff.Mode.SRC_IN);
        progressDoalog.setContentView(progressBar);
        return progressDoalog;
    }

    public static void dismissProgressDialog(ProgressDialog progressDoalog) {
        if (progressDoalog != null)
            progressDoalog.dismiss();
    }

    public static String getRealPathFromURI(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (uri != null) {
            if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    if (new File(Environment.getExternalStorageDirectory() + "/" + split[1]).exists()) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    } else if (new File("storage/" + split[0] + "/" + split[1]).exists()) {
                        return "storage/" + split[0] + "/" + split[1];
                    } else {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                } else if (isDownloadsDocument(uri)) {
                    final String id = DocumentsContract.getDocumentId(uri);
                    uri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                } else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("image".equals(type)) {
                        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    selection = "_id=?";
                    selectionArgs = new String[]{
                            split[1]
                    };
                }
            }

            if ("content".equalsIgnoreCase(uri.getScheme())) {
                String[] projection = {
                        MediaStore.Images.Media.DATA
                };
                Cursor cursor = null;
                try {
                    cursor = context.getContentResolver()
                            .query(uri, projection, selection, selectionArgs, null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    if (cursor.moveToFirst()) {
                        return cursor.getString(column_index);
                    }
                } catch (Exception e) {
                }
            } else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static void shareApp(Context context, String s) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
            String sAux = s + "\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + context.getPackageName() + "\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            context.startActivity(Intent.createChooser(i, "Share with.."));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public static void rateApp(Context context) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName()));
            context.startActivity(i);
        } catch (Exception e) {
            //e.toString();
        }
    }

    public static String setDateShortFormate(String dateTime) {
        try {
            Date date = serverDate.parse(dateTime);
            dateTime = inputFormate1.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    public static String compareDate(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

            Date currentDate = sdf.parse(sdf.format(Calendar.getInstance().getTime()));
            Date date2 = sdf.parse(dateTime);
            if (currentDate.compareTo(date2) > 0) {
                // 2  is after 1
                Log.e("Tag app", "currentDate is after Date2");
                return "0";
            } else if (currentDate.compareTo(date2) < 0) {
                Log.e("Tag app","currentDate is before Date2");
                return "0";
            } else if (currentDate.compareTo(date2) == 0) {
                // 11 - 2
                Log.e("Tag app", "currentDate is equal to Date2");
                return "1";
            }else {
                return "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateTime;
    }
}
