package com.boredabroad.global;

import android.app.Application;

import androidx.multidex.MultiDexApplication;

import com.boredabroad.model.CountryData;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Assure on 02-07-2018.
 */

public class GlobalApplication extends MultiDexApplication {
   public static int MESSAGE_PAGINATION=5;
   public static int RESULT_LOCATION=2030;
   public static final int NOTIFICATION_ID = 100;
   public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
   public static  String NotiData = "";
   public static String CategoryList = "CategoryList";
   public static String auth="simplerestapi",client="frontend-client";
   public static final String REGISTRATION_COMPLETE = "registrationComplete";
   public static final String SHARED_PREF = "ah_firebase";
   public static JSONArray jsonArrayCountry;
   public static ArrayList<CountryData> countryDataArrayList=new ArrayList<>();

   @Override
   public void onCreate() {
      super.onCreate();

      ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
              .threadPriority(Thread.NORM_PRIORITY - 2)
              .denyCacheImageMultipleSizesInMemory()
              .diskCacheFileNameGenerator(new Md5FileNameGenerator())
              .diskCacheSize(50 * 1024 * 1024)
              .tasksProcessingOrder(QueueProcessingType.LIFO)
              .writeDebugLogs() // Remove for release app
              .build();
      ImageLoader.getInstance().init(config);

      try {
         jsonArrayCountry=new JSONArray(getAllJSonPackage());
         countryDataArrayList.clear();
         for(int i=0;i<jsonArrayCountry.length();i++){
            countryDataArrayList.add(new CountryData(
                    jsonArrayCountry.getJSONObject(i).getJSONArray("callingCodes").getString(0)+"",
                    jsonArrayCountry.getJSONObject(i).getString("name")+"",
                    jsonArrayCountry.getJSONObject(i).getString("flag")+"",
                    jsonArrayCountry.getJSONObject(i).getString("alpha2Code")+"",
                    jsonArrayCountry.getJSONObject(i).getString("alpha3Code")+""
            ));
         }



      } catch (JSONException e) {
         e.printStackTrace();
      }

   }

   public String getAllJSonPackage() {
      String json = null;
      try {
         InputStream is = getAssets().open("all_country.json");
         int size = is.available();
         byte[] buffer = new byte[size];
         is.read(buffer);
         is.close();
         json = new String(buffer, "UTF-8");
      } catch (IOException ex) {
         ex.printStackTrace();
         return null;
      }
      return json;
   }

}
