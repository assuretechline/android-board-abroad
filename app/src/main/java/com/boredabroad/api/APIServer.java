package com.boredabroad.api;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.boredabroad.global.GlobalApplication;
import com.jacksonandroidnetworking.JacksonParserFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Properties;


public class APIServer {

    Context context;
    Properties properties;
    String subUrl = "";
    String mainUrl = "";

    public APIServer(Context context) {
        this.context = context;
        properties = new LoadAssetProperties().loadRESTApiFile(context.getResources(), "rest.properties", context);
        mainUrl = properties.getProperty("MainUrl");
        AndroidNetworking.setParserFactory(new JacksonParserFactory());
    }

    public void verifyMobile(final APIResponse listener, String mobile, String countryCode) {
        subUrl = properties.getProperty("verify_mobile") +"?mobile=" + mobile + "&code=" + countryCode;
        //Log.e("Tag","URL : "+mainUrl+subUrl);
       // subUrl = properties.getProperty("verify_mobile");
        AndroidNetworking.get(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void userDetailMobile(final APIResponse listener, String mobile, String countryCode) {
        subUrl = properties.getProperty("user_detail_mobile")+"?mobile=" + mobile + "&code=" + countryCode;
        //Log.e("Tag","URL  : " +mainUrl+subUrl );
        AndroidNetworking.get(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void sendWave(final APIResponse listener, String sender, String receiver) {
        subUrl = properties.getProperty("send_wave")+ receiver + "/" + sender;
        //Log.e("Tag","URL  : " +mainUrl+subUrl );
        AndroidNetworking.get(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void getCurrentCountry(final APIResponse listener) {

        //Log.e("Tag","URL  : " +mainUrl+subUrl );
        AndroidNetworking.get("http://ip-api.com/json")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void addUser(final APIResponse listener, String name, String email, String password, String mobile,String code, String countryName) {
        subUrl = properties.getProperty("add_user");
        AndroidNetworking.post(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addBodyParameter("name", name)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .addBodyParameter("mobile", mobile)
                .addBodyParameter("code", code)
                .addBodyParameter("country", countryName)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void updateUser(final APIResponse listener, String name, String email, String password, String mobile, String userID, String countryName, String code) {
        subUrl = properties.getProperty("add_user") + "/" + userID;
        AndroidNetworking.post(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addBodyParameter("name", name)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .addBodyParameter("mobile", mobile)
                .addBodyParameter("country", countryName)
                .addBodyParameter("code", code)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void addDevice(final APIResponse listener, String user_id, String device_type, String app_version, String device_model, String os_version, String device_id, String device_token) {
        subUrl = properties.getProperty("add_device");

        // Log.e("Tag","Data : "+)

        AndroidNetworking.post(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("device_type", device_type)
                .addBodyParameter("app_version", app_version)
                .addBodyParameter("device_model", device_model)
                .addBodyParameter("os_version", os_version)
                .addBodyParameter("device_id", device_id)
                .addBodyParameter("device_token", device_token)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getActivity(final APIResponse listener) {
        subUrl = properties.getProperty("activity");
        AndroidNetworking.get(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void getTripList(final APIResponse listener, String lat, String lang) {
        subUrl = properties.getProperty("gmap") + "?lat=" + lat + "&long=" + lang;
        AndroidNetworking.get(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void getInstaToken(final APIResponse listener, String url) {

        AndroidNetworking.get(url)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void getUserDetail(final APIResponse listener, String userId) {
        subUrl = properties.getProperty("user_detail") + userId;
        AndroidNetworking.get(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void addTrip(final APIResponse listener, String title, String date, String type, String latitude, String longitude, String privacy, String activity, String city, String state, String country, String userid) {
        subUrl = properties.getProperty("trip_add");
        AndroidNetworking.post(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addBodyParameter("title", title)
                .addBodyParameter("date", date)
                .addBodyParameter("type", type)
                .addBodyParameter("latitude", latitude)
                .addBodyParameter("longitude", longitude)
                .addBodyParameter("privacy", privacy)
                .addBodyParameter("activity", activity)
                .addBodyParameter("city", city)
                .addBodyParameter("state", state)
                .addBodyParameter("country", country)
                .addBodyParameter("userid", userid)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void deleteProfileUser(final APIResponse listener, String url, String id) {
        subUrl = properties.getProperty("user_profile_delete");
        AndroidNetworking.post(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addBodyParameter("url", url)
                .addBodyParameter("userid", id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void addSocial(final APIResponse listener, String userid, String key, String value) {
        subUrl = properties.getProperty("add_social");
        AndroidNetworking.post(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addBodyParameter("userid", userid)
                .addBodyParameter("key", key)
                .addBodyParameter("value", value)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void removeSocial(final APIResponse listener, String userid, String key) {
        Log.e("Tag","User ID : "+userid);
        Log.e("Tag","key : "+key);
        subUrl = properties.getProperty("disconnect");
        AndroidNetworking.post(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addBodyParameter("user_id", userid)
                .addBodyParameter("key", key)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }


    public void userProfileImage(final APIResponse listener, String url, String userid) {
        subUrl = properties.getProperty("user_profile");
       /* AndroidNetworking.upload(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addMultipartFile("url[]", url)
                .addMultipartParameter("userid", userid)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });*/
    }

    public void checkAppUpdate(final APIResponse listener) {
        subUrl = properties.getProperty("deviceinfo");
        AndroidNetworking.get(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void getProfile(final APIResponseArray listener, String studId) {

        subUrl = properties.getProperty("profile") + studId;

        AndroidNetworking.get(mainUrl + subUrl)
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        listener.onSuccessArray(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailureArray(anError.getErrorBody());
                    }
                });
    }

}
