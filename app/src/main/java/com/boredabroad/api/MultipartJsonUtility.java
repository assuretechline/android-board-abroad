package com.boredabroad.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * This utility class provides an abstraction layer for sending multipart HTTP
 * POST requests to a web server.
 *
 * @author www.codejava.net
 */
public class MultipartJsonUtility {
    private final String boundary;
    private static final String LINE_FEED = "\r\n";
    private HttpURLConnection httpConn;
    private String charset;
    private OutputStream outputStream;
    private DataOutputStream dataOutputStream;

    private PrintWriter writer;

    /**
     * This constructor initializes a new HTTP POST request with content type
     * is set to multipart/form-data
     *
     * @param requestURL
     * @param charset
     * @throws IOException
     */
    public MultipartJsonUtility(String requestURL, String charset, String access_token)
            throws IOException {
        this.charset = charset;

        // creates a unique boundary based on time stamp
        boundary = "===" + System.currentTimeMillis() + "===";

        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true); // indicates POST method
        httpConn.setDoInput(true);
        httpConn.setRequestMethod("POST");
        httpConn.setRequestProperty("Connection", "Keep-Alive");
        // httpConn.setRequestProperty("ENCTYPE", "multipart/form-data");
        httpConn.setRequestProperty("Content-Type",
                "application/json");
        httpConn.setRequestProperty("Accept", "*/*");
        outputStream = httpConn.getOutputStream();
        dataOutputStream = new DataOutputStream(outputStream);

        writer = new PrintWriter(new OutputStreamWriter(outputStream, charset),
                true);
    }


    /**
     * Adds a JSON field to the request
     *
     * @param value field value
     */
    public void addJSONField(String value) {
        try {
            dataOutputStream.write(value.getBytes("UTF-8"));
            dataOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Completes the request and receives response from the server.
     *
     * @return a list of Strings as response in case the server returned
     * status OK, otherwise an exception is thrown.
     * @throws IOException
     */
    public String finish() throws IOException {
        List<String> response = new ArrayList<String>();
        String temp = "";


        // checks server's status code first
        int status = httpConn.getResponseCode();
        String serverResponseMessage = httpConn.getResponseMessage().toString();
//        Log.e("Tag -> ", serverResponseMessage);
//        Log.e("Tag -> ", status + " - " + HttpURLConnection.HTTP_CREATED);
        if (status == HttpURLConnection.HTTP_CREATED || status == HttpURLConnection.HTTP_OK || status == 500) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpConn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                //Log.e("Tag -> ",line);
                response.add(line);
                temp += line;
            }
            reader.close();
            httpConn.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        return temp;
    }
}
