package com.boredabroad;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.boredabroad.fragment.SocialFragment;
import com.boredabroad.global.Utility;


public class SocialActivity extends BaseActivity implements View.OnClickListener {

    Context context;
    ImageView imgv_back;
    FrameLayout fl_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        Utility.setStatusColor(this);
        setView();
        setData();
        setListener();
        setColor();
    }

    private void setView() {
        context = this;
        imgv_back = findViewById(R.id.imgv_back);
        fl_main = findViewById(R.id.fl_main);
    }

    private void setData() {
        getSupportFragmentManager().beginTransaction().replace(fl_main.getId(), new SocialFragment(true), "SocialFragment").addToBackStack("SocialFragment").commitAllowingStateLoss();
    }

    private void setListener() {
        imgv_back.setOnClickListener(this);
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.font_saff));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        Utility.gotoBack(context);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


}
