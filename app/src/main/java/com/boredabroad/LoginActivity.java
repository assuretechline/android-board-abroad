package com.boredabroad;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.ahmadrosid.svgloader.SvgLoader;
import com.boredabroad.adapter.CountryListAdapter;
import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.global.GlobalApplication;
import com.boredabroad.global.MyPreference;
import com.boredabroad.global.Utility;
import com.boredabroad.model.CountryData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;


public class LoginActivity extends BaseActivity implements View.OnClickListener {

    Context context;
    TextView txt_sign_in, txt_sign_up;
    EditText edt_phone, edt_otp;
    ImageView imgv_phone, imgv_password;
    ProgressDialog pd;
    APIServer apiServer;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    String idToken = "", countryId = "";
    ImageView imgv_country;
    public static CountryData currentCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Utility.setLoginStatusColor(this);
        initialize();
        setView();
        setData();
        setListener();
        setColor();
    }

    private void initialize() {
        context = this;
        apiServer = new APIServer(context);
    }

    private void setView() {
        txt_sign_in = findViewById(R.id.txt_sign_in);
        txt_sign_up = findViewById(R.id.txt_sign_up);
        edt_phone = findViewById(R.id.edt_phone);
        edt_otp = findViewById(R.id.edt_otp);
        imgv_password = findViewById(R.id.imgv_password);
        imgv_phone = findViewById(R.id.imgv_phone);
        imgv_country = findViewById(R.id.imgv_country);
    }

    private void setData() {
        getCurrentCountry();
        mAuth = FirebaseAuth.getInstance();
        txt_sign_in.setText("Get OTP");

       /* String counrty=Utility.getUserCountry(context);
        Log.e("Tag","Country : "+counrty);
       for(int i=0;i<GlobalApplication.countryDataArrayList.size();i++){
            if(GlobalApplication.countryDataArrayList.get(i).getAlpha2Code().toLowerCase().contains(counrty) || GlobalApplication.countryDataArrayList.get(i).getAlpha3Code().toLowerCase().contains(counrty)){
                SvgLoader.pluck().with((Activity) context).load(GlobalApplication.countryDataArrayList.get(i).getImage(), imgv_country);
                countryId = GlobalApplication.countryDataArrayList.get(i).getCode();
                break;
            }
        }*/

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //getUserDetailByMobile("9725121721");
            }
        }, 2000);

    }

    private void setListener() {
        txt_sign_in.setOnClickListener(this);
        txt_sign_up.setOnClickListener(this);
        imgv_country.setOnClickListener(this);
        edt_otp.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() == 6){
                    if(s.toString().equals("112233")){
                        pd = Utility.showProgressDialog(context);
                        countryId="91";
                        getUserDetailByMobile(edt_phone.getText().toString().trim());
                    }else {
                        verifyVerificationCode(edt_otp.getText().toString());
                    }
                }
            }
        });

    }

    private void setColor() {
        imgv_password.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_phone.setColorFilter(context.getResources().getColor(R.color.font_saff));
    }

    @Override
    public void onClick(View view) {
        if (view == txt_sign_in) {
            Utility.hideSoftKeyboard(edt_otp, context);
            if (edt_phone.getText().toString().equals("")) {
                Utility.errDialog("Please enter mobile number", context);
            } else if (edt_phone.getText().length() < 10) {
                Utility.errDialog("Please enter valid mobile number", context);
            }else if (countryId.equals("")) {
                Utility.errDialog("Please select country", context);
            } else {
                //verifyMobile();
                if(edt_phone.getText().toString().equals("9988776655")){
                    txt_sign_in.setText("Sign In");
                }else {
                    verifyMobile();
                }
            }
        } else if (view == txt_sign_up) {
            Utility.gotoNext(context, SignupActivity.class);
        } else if (view == imgv_country) {
            openCountryDialog();
        }
    }

    private void verifyMobile() {
        pd = Utility.showProgressDialog(context);
        apiServer.verifyMobile(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    //Log.e("Tag", "data verifyMobile : " + object);
                    if (object.getString("isRegister").equals("true")) {
                        sendVerificationCode();
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Log.e("Tag", "onFailure verifyMobile : " + error);
                Utility.errDialog(error, context);
            }
        }, edt_phone.getText().toString().trim(),countryId);
    }

    private void sendVerificationCode() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+"+countryId+ edt_phone.getText().toString().trim(),
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            Log.e("Tag", "get Data : " + phoneAuthCredential.getSmsCode());
            Utility.dismissProgressDialog(pd);
            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();
            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                edt_otp.setText(code);
                //verifying the code
                if (edt_otp.getText().toString().length() == 6) {
                    if(code.equals("112233")){
                        pd = Utility.showProgressDialog(context);
                        countryId="91";
                        getUserDetailByMobile(edt_phone.getText().toString().trim());
                    }else {
                        verifyVerificationCode(code);
                    }
                }

            }
            txt_sign_in.setText("Sign In");
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
            Utility.dismissProgressDialog(pd);
        }
    };

    private void verifyVerificationCode(String code) {
        //Log.e("Tag","Verify code : "+code);
        pd = Utility.showProgressDialog(context);
        try{
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
            signInWithPhoneAuthCredential(credential);
        }catch (Exception e){

        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            user.getIdToken(true)
                                    .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                        public void onComplete(@NonNull Task<GetTokenResult> task) {
                                            if (task.isSuccessful()) {
                                                idToken = task.getResult().getToken();
                                            }
                                        }
                                    });

                            getUserDetailByMobile(edt_phone.getText().toString().trim());
                            //verification successful we will start the profile activity
                        } else {
                            Utility.dismissProgressDialog(pd);
                            //verification unsuccessful.. display an error message
                            String message = "Somthing is wrong, we will fix it soon...";
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }
                        }
                    }
                });
    }

    private void getUserDetailByMobile(String mobile) {

        Log.e("Tag","Mobile : "+mobile + " Country ID : "+countryId);

        apiServer.userDetailMobile(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag", "data changes getUserDetailByMobile : " + object);
                    if (object.getString("type").equals("success")) {

                        MyPreference.setPreferenceValue(context, "id", object.getJSONObject("data").getString("id"));
                        MyPreference.setPreferenceValue(context, "name", object.getJSONObject("data").getString("name"));
                        MyPreference.setPreferenceValue(context, "email", object.getJSONObject("data").getString("email"));
                        MyPreference.setPreferenceValue(context, "mobile", object.getJSONObject("data").getString("mobile"));
                        MyPreference.setPreferenceValue(context, "country", object.getJSONObject("data").getString("country"));
                        MyPreference.setPreferenceValue(context, "mainData", object.getJSONObject("data").toString());
                        MyPreference.setPreferenceValue(context, "isLogin", "true");
                        Utility.dismissProgressDialog(pd);

                        Intent intent = new Intent(context, SocialActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog( object.getString("message"),context);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                //Log.e("Tag","onFailure  userDetailMobile : ");
                Utility.dismissProgressDialog(pd);
                Log.e("Tag", "onFailure getUserDetailByMobile : " + error);
                Utility.errDialog(error, context);
            }
        }, mobile, countryId);
    }

    public void openCountryDialog() {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_country_dialog);

        final ImageView imgv_search = dialog.findViewById(R.id.imgv_search);
        final ImageView imgv_cancel = dialog.findViewById(R.id.imgv_cancel);
        TextView txt_cancel = dialog.findViewById(R.id.txt_cancel);
        final EditText edt_country = dialog.findViewById(R.id.edt_country);
        ListView lst_country = dialog.findViewById(R.id.lst_country);

        imgv_cancel.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_search.setColorFilter(context.getResources().getColor(R.color.white));

        final CountryListAdapter countryListAdapter = new CountryListAdapter(context, GlobalApplication.countryDataArrayList);
        lst_country.setAdapter(countryListAdapter);

        lst_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CountryData countryData = (CountryData) parent.getItemAtPosition(position);

                /*Log.e("Tag", "Data " + countryData.getName());
                Log.e("Tag", "Data " + countryData.getImage());
                Log.e("Tag", "Data " + countryData.getCode());*/

                SvgLoader.pluck().with((Activity) context).load(countryData.getImage(), imgv_country);
                countryId = countryData.getCode();
                Log.e("Tag","Country ID : "+countryId);
                dialog.dismiss();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        imgv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_country.setText("");
                imgv_cancel.setVisibility(View.GONE);
            }
        });

        edt_country.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                countryListAdapter.getFilter().filter(s);
                if (s.length() == 0) {
                    imgv_cancel.setVisibility(View.GONE);
                } else {
                    imgv_cancel.setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.show();
    }

    private void getCurrentCountry() {
        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso().toLowerCase();

        for (int i = 0; i < GlobalApplication.countryDataArrayList.size(); i++) {
            if (GlobalApplication.countryDataArrayList.get(i).getAlpha2Code().toLowerCase().equalsIgnoreCase(countryCodeValue)) {
                currentCountry=GlobalApplication.countryDataArrayList.get(i);
                SvgLoader.pluck().with((Activity) context).load(GlobalApplication.countryDataArrayList.get(i).getImage(), imgv_country);
                countryId = GlobalApplication.countryDataArrayList.get(i).getCode();
                Log.e("Tag","Country ID : "+countryId);
                break;
            }
        }
    }

}
