package com.boredabroad;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.boredabroad.fragment.AddTripFragment;
import com.boredabroad.fragment.SocialFragment;
import com.boredabroad.global.Utility;


public class AddTripActivity extends BaseActivity implements View.OnClickListener {

    Context context;
    ImageView imgv_back;
    FrameLayout fl_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        Utility.setStatusColor(this);
        setView();
        setData();
        setListener();
        setColor();
    }

    private void setView() {
        context = this;
        imgv_back = findViewById(R.id.imgv_back);
        fl_main = findViewById(R.id.fl_main);
    }

    private void setData() {
        getSupportFragmentManager().beginTransaction().replace(fl_main.getId(), new AddTripFragment(false), "AddTripFragment").addToBackStack("AddTripFragment").commitAllowingStateLoss();
    }

    private void setListener() {
        imgv_back.setOnClickListener(this);
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.font_saff));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        Utility.gotoBack(context);
    }
}
