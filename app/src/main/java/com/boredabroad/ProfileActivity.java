package com.boredabroad;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.fragment.MapFragment;
import com.boredabroad.global.MyPreference;
import com.boredabroad.global.Utility;
import com.boredabroad.model.UserInfoData;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileActivity extends BaseActivity implements View.OnClickListener {

    Context context;
    TextView txt_name;
    FrameLayout fl_view_photo;
    ImageView imgv_back, imgv_previous, imgv_next, imgv_wave, imgv_email, imgv_ws, imgv_fb, imgv_twitter, imgv_insta;
    CircleImageView imgv_profile;
    LinearLayout ll_wave, ll_email, ll_ws, ll_fb, ll_twitter, ll_insta;
    UserInfoData userInfoData;
    int currentPos = 0;
    JSONArray jsonArrayImage;
    APIServer apiServer;
    ProgressDialog pd;
    String userId = "";
    String ws = "", insta = "", fb = "", twitter = "", mail = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Utility.setLoginStatusColor(this);
        initialize();
        setView();
        setData();
        setListener();
        setColor();
    }

    private void initialize() {
        context = this;
        apiServer = new APIServer(context);
    }

    private void setView() {

        txt_name = findViewById(R.id.txt_name);
        imgv_next = findViewById(R.id.imgv_next);
        imgv_previous = findViewById(R.id.imgv_previous);
        imgv_wave = findViewById(R.id.imgv_wave);
        imgv_email = findViewById(R.id.imgv_email);
        imgv_ws = findViewById(R.id.imgv_ws);
        imgv_fb = findViewById(R.id.imgv_fb);
        imgv_twitter = findViewById(R.id.imgv_twitter);
        imgv_insta = findViewById(R.id.imgv_insta);
        imgv_profile = findViewById(R.id.imgv_profile);
        ll_wave = findViewById(R.id.ll_wave);
        imgv_back = findViewById(R.id.imgv_back);
        ll_email = findViewById(R.id.ll_email);
        ll_ws = findViewById(R.id.ll_ws);
        ll_fb = findViewById(R.id.ll_fb);
        ll_twitter = findViewById(R.id.ll_twitter);
        ll_insta = findViewById(R.id.ll_insta);
        fl_view_photo = findViewById(R.id.fl_view_photo);
    }

    private void setData() {


        ll_insta.setAlpha(0.2f);
        ll_twitter.setAlpha(0.2f);
        ll_ws.setAlpha(0.2f);


        ImageLoader.getInstance().displayImage("", imgv_profile);
        currentPos = 0;
        String image = "";

        try {
            userInfoData = MapFragment.userInfoDataArrayList.get(getIntent().getIntExtra("data", 0));
            try {
                userId = userInfoData.getId();

                jsonArrayImage = new JSONArray(userInfoData.getImage());
                if (jsonArrayImage == null || jsonArrayImage.length() == 0) {
                    image = context.getString(R.string.avtar_image);
                } else {
                    image = jsonArrayImage.getJSONObject(0).getString("thumb");
                }
            } catch (JSONException e) {
                image = context.getString(R.string.avtar_image);
                e.printStackTrace();
            }

            ImageLoader.getInstance().displayImage(image, imgv_profile);
            txt_name.setText(userInfoData.getName());
            imgv_previous.setAlpha(0.5f);
            if (jsonArrayImage.length() <= 1) {
                imgv_next.setAlpha(0.5f);
            }

            Log.e("Tag", "Social : " + userInfoData.getSocial());

            fb = new JSONObject(userInfoData.getSocial()).getJSONObject("social").getString("facebook");
            insta = new JSONObject(userInfoData.getSocial()).getJSONObject("social").getString("instagram");
            twitter = new JSONObject(userInfoData.getSocial()).getJSONObject("social").getString("twitter");
            ws = new JSONObject(userInfoData.getSocial()).getJSONObject("social").getString("whatsapp");
            mail = new JSONObject(userInfoData.getSocial()).getString("email");

        } catch (Exception e) {
            Log.e("Tag", "Eception e : " + e.getMessage());
            try {
                JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("notiData"));
                try {
                    jsonArrayImage = jsonObject.getJSONArray("image");
                    if (jsonArrayImage == null || jsonArrayImage.length() == 0) {
                        image = context.getString(R.string.avtar_image);
                    } else {
                        image = jsonArrayImage.getJSONObject(0).getString("thumb");
                    }
                } catch (JSONException e1) {
                    image = context.getString(R.string.avtar_image);
                    e1.printStackTrace();
                }
                ImageLoader.getInstance().displayImage(image, imgv_profile);

                txt_name.setText(jsonObject.getString("name"));
                userId = jsonObject.getString("id");
                imgv_previous.setAlpha(0.5f);
                if (jsonArrayImage.length() <= 1) {
                    imgv_next.setAlpha(0.5f);
                }


                fb = jsonObject.getJSONObject("social").getString("facebook");
                insta = jsonObject.getJSONObject("social").getString("instagram");
                twitter = jsonObject.getJSONObject("social").getString("twitter");
                ws = jsonObject.getJSONObject("social").getString("whatsapp");
                mail = jsonObject.getString("email");
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }

        if (fb.equals("")) {
            ll_fb.setAlpha(0.2f);
        } else {
            ll_fb.setAlpha(1f);
        }

        if (insta.equals("")) {
            ll_insta.setAlpha(0.2f);
        } else {
            ll_insta.setAlpha(1f);
        }
        if (twitter.equals("")) {
            ll_twitter.setAlpha(0.2f);
        } else {
            ll_twitter.setAlpha(1f);
        }
        if (ws.equals("")) {
            ll_ws.setAlpha(0.2f);
        } else {
            ll_ws.setAlpha(1f);
        }
    }

    private void setListener() {
        ll_fb.setOnClickListener(this);
        ll_email.setOnClickListener(this);
        ll_ws.setOnClickListener(this);
        ll_insta.setOnClickListener(this);
        ll_twitter.setOnClickListener(this);
        imgv_next.setOnClickListener(this);
        imgv_previous.setOnClickListener(this);
        imgv_back.setOnClickListener(this);
        ll_wave.setOnClickListener(this);
        fl_view_photo.setOnClickListener(this);
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.saffron));
        imgv_next.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_previous.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_email.setColorFilter(context.getResources().getColor(R.color.email));
        imgv_wave.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_ws.setColorFilter(context.getResources().getColor(R.color.ws));
        imgv_fb.setColorFilter(context.getResources().getColor(R.color.fb));
        imgv_twitter.setColorFilter(context.getResources().getColor(R.color.twitter));
        //imgv_insta.setColorFilter(context.getResources().getColor(R.color.white));
    }

    @Override
    public void onClick(View view) {
        if (view == txt_name) {

        } else if (view == imgv_back) {
            onBackPressed();
        } else if (view == ll_ws) {

        } else if (view == ll_fb) {
            try {
                String id = fb;
                if (!id.equals("")) {
                    try {
                        ll_fb.setAlpha(1f);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://messaging/" + id)));
                    } catch (Exception e) {

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (view == ll_insta) {
            try {
                String id = insta;
                if (!id.equals("")) {
                    ll_insta.setAlpha(1f);
                    Uri uri = Uri.parse("http://instagram.com/_u/" + id);
                    Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                    likeIng.setPackage("com.instagram.android");

                    try {
                        startActivity(likeIng);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://instagram.com/" + id)));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (view == ll_twitter) {
            try {
                String id = twitter;
                if (!id.equals("")) {
                    ll_twitter.setAlpha(1f);
                    Intent intent = null;
                    try {
                        // get the Twitter app if possible
                        getPackageManager().getPackageInfo("com.twitter.android", 0);
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=" + id));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        this.startActivity(intent);
                    } catch (Exception e) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + "USERID"));
                        startActivity(intent);
                        Log.e("Tag", "Exception : " + e.getMessage());
                        // no Twitter app, revert to browser
                        //intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/PROFILENAME"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (view == ll_ws) {
            try {
                String contact = ws;
                ll_ws.setAlpha(1f);
                Log.e("Tag", "NUmber : " + contact);
                if (!contact.equals("")) {
                    String url = "https://api.whatsapp.com/send?phone=" + contact;
                    try {
                        PackageManager pm = context.getPackageManager();
                        pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    } catch (PackageManager.NameNotFoundException e) {
                        Toast.makeText(context, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (view == ll_email) {
            Log.e("Tag","Email : "+mail);
            try {
                String id = mail;
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", id, null));
                intent.putExtra("android.intent.extra.SUBJECT", "");
                startActivity(Intent.createChooser(intent, "Select an email client"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (view == imgv_next) {
            if (currentPos != jsonArrayImage.length() - 1) {
                imgv_next.setAlpha(1f);
            }
            if (currentPos == jsonArrayImage.length() - 1) {

            } else {
                currentPos = currentPos + 1;
                try {
                    ImageLoader.getInstance().displayImage(jsonArrayImage.getJSONObject(currentPos).getString("thumb"), imgv_profile);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (currentPos == jsonArrayImage.length() - 1) {
                    imgv_next.setAlpha(0.5f);
                } else {
                    imgv_next.setAlpha(1f);
                }
                if (currentPos == 0) {
                    imgv_previous.setAlpha(0.5f);
                } else {
                    imgv_previous.setAlpha(1f);
                }
            }
        } else if (view == imgv_previous) {
            if (currentPos != 0) {
                imgv_previous.setAlpha(1f);
            }

            if (currentPos == 0) {

            } else {
                currentPos = currentPos - 1;
                try {
                    ImageLoader.getInstance().displayImage(jsonArrayImage.getJSONObject(currentPos).getString("thumb"), imgv_profile, Utility.getUserProfileOption());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (currentPos == 0) {
                    imgv_previous.setAlpha(0.5f);
                } else {
                    imgv_previous.setAlpha(1f);
                }
                if (currentPos == jsonArrayImage.length() - 1) {
                    imgv_next.setAlpha(0.5f);
                } else {
                    imgv_next.setAlpha(1f);
                }
            }
        } else if (view == ll_wave) {
            sendwave();
        } else if (view == fl_view_photo) {
            Intent i = new Intent(context, PhotoDetailActivity.class);
            i.putExtra("data", userInfoData.getImage());
            context.startActivity(i);
            ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
        }
    }

    @Override
    public void onBackPressed() {
        Utility.gotoBack(context);
    }

    private void sendwave() {
        pd = Utility.showProgressDialog(context);
        apiServer.sendWave(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {

                    if (object.getInt("status") == 200) {
                        Utility.errDialog(object.getString("message"), context);
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                //Log.e("Tag","onFailure  userDetailMobile : ");
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, MyPreference.getPreferenceValue(context, "id").toString(), userId);
    }

}
