package com.boredabroad.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.boredabroad.R;
import com.boredabroad.model.TripListData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class AllTripAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<TripListData> list;

    public AllTripAdapter(Context context, ArrayList<TripListData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_all_trip, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_name = view.findViewById(R.id.txt_name);
        holder.txt_location = view.findViewById(R.id.txt_location);
        holder.txt_date = view.findViewById(R.id.txt_date);
        holder.txt_activity = view.findViewById(R.id.txt_activity);
    }

    @SuppressLint("NewApi")
    private void setData(final Holder holder, final int position) {

        holder.txt_name.setText(list.get(position).getName());
        holder.txt_activity.setText(list.get(position).getActivity());
        holder.txt_date.setText(list.get(position).getDate());
        holder.txt_location.setText(list.get(position).getLocation());

    }


    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    private class Holder {
        TextView txt_name,txt_date,txt_activity,txt_location;
    }
}
