package com.boredabroad.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.boredabroad.fragment.GalleryDetailFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by home on 16/06/18.
 */

public class GalleryPagerAdapter extends FragmentPagerAdapter {


    JSONArray gallerySubs;

    public GalleryPagerAdapter(FragmentManager fm, JSONArray gallerySubs) {
        super(fm);
        this.gallerySubs = gallerySubs;
    }

    @Override
    public Fragment getItem(int i) {

        Fragment fragment = null;
        try {
            fragment = new GalleryDetailFragment(gallerySubs.getJSONObject(i).getString("orignal"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return gallerySubs.length();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (position+1) + "/" + gallerySubs.length();
    }

}
