package com.boredabroad.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.boredabroad.R;
import com.boredabroad.model.MenuData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class MonthAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<MenuData> list;

    public MonthAdapter(Context context, ArrayList<MenuData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_month, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_title = view.findViewById(R.id.txt_title);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, final int position) {
        holder.txt_title.setText(list.get(position).getTitle());
        if (list.get(position).isSelected()) {
            holder.txt_title.setBackground(ContextCompat.getDrawable(context, R.drawable.ring_saffron));
        } else {
            holder.txt_title.setBackground(null);
        }

        holder.txt_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i=0;i<list.size();i++){
                    list.get(i).setSelected(false);
                }
                list.get(position).setSelected(true);
                notifyDataSetChanged();
            }
        });
    }


    private void initialize() {
    }

    private class Holder {
        TextView txt_title;
    }
}
