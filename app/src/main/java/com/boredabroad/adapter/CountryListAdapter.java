package com.boredabroad.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.boredabroad.R;
import com.boredabroad.model.CountryData;
import com.boredabroad.model.MenuData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class CountryListAdapter extends BaseAdapter implements Filterable {
    ImageLoader imageLoader;
    Context context;
    ArrayList<CountryData> list;
    ArrayList<CountryData> listTemp;
    ValueFilter valueFilter = new ValueFilter();

    public CountryListAdapter(Context context, ArrayList<CountryData> list) {
        this.context = context;
        this.list = list;
        this.listTemp = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return  list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_country, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_title = view.findViewById(R.id.txt_title);
        holder.imgv_icon = view.findViewById(R.id.imgv_icon);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
        try {

            holder.txt_title.setText(list.get(position).getName());
            SvgLoader.pluck().with((Activity) context).load(list.get(position).getImage(), holder.imgv_icon);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public Filter getFilter() {
        return valueFilter;
    }

    private class Holder {
        TextView txt_title;
        ImageView imgv_icon;
    }

    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<CountryData> filterList = new ArrayList<CountryData>();
                for (int i = 0; i < listTemp.size(); i++) {
                    if (listTemp.get(i).getName().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        filterList.add(listTemp.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = listTemp.size();
                results.values = listTemp;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            list = (ArrayList<CountryData>) results.values;
            notifyDataSetChanged();
        }
    }

}
