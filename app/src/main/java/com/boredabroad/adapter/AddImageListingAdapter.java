package com.boredabroad.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.boredabroad.R;
import com.boredabroad.api.APIServer;
import com.boredabroad.fragment.SettingFragment;
import com.boredabroad.global.Utility;
import com.boredabroad.model.AddImageListingData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by home on 07/07/18.
 */

public class AddImageListingAdapter extends RecyclerView.Adapter<AddImageListingAdapter.HorizontalViewHolder> {

    public ArrayList<AddImageListingData> list;
    Context context;
    ImageLoader imageLoader;
    APIServer apiServer;
    SettingFragment settingFragment;

    public AddImageListingAdapter(Context context, ArrayList<AddImageListingData> list, SettingFragment settingFragment) {
        this.list = list;
        this.context = context;
        this.settingFragment = settingFragment;
        initialize();
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
    }

    @Override
    public HorizontalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_add_image_listing, parent, false);
        return new HorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HorizontalViewHolder holder, final int position) {
        setData(holder, position);
        setColor(holder);
    }

    public void setData(final HorizontalViewHolder holder, final int position) {
        holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.saffron));
        if (list.get(position).getId().equals("")) {
            imageLoader.displayImage(list.get(position).getImgLoad(), holder.imgv_prop, Utility.getUserProfileOption());
        } else {
            imageLoader.displayImage(list.get(position).getId(), holder.imgv_prop, Utility.getUserProfileOption());
        }

        /*imageLoader.displayImage(list.get(position).getImgLoad(), holder.imgv_prop, Utility.getUserProfileOption(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) { }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });*/


        holder.imgv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultDialogWithYesNo("Are you sure?", context, position);
            }
        });

        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void setColor(HorizontalViewHolder holder) {
        //holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.white_image));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HorizontalViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgv_prop;
        ImageView imgv_delete;
        LinearLayout ll_main;

        public HorizontalViewHolder(View itemView) {
            super(itemView);
            imgv_prop = itemView.findViewById(R.id.imgv_prop);
            imgv_delete = itemView.findViewById(R.id.imgv_delete);
            ll_main = itemView.findViewById(R.id.ll_main);
        }
    }

    public void defaultDialogWithYesNo(String error, Context context, final int position) {
        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
        alertDialog2.setTitle("Alert!");
        alertDialog2.setMessage(error);

        alertDialog2.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        settingFragment.deleteImage(position);
                    }
                });

        alertDialog2.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });

        alertDialog2.show();
    }


    private void deleteImage(String id, String image, final int position) {
     /*   pd = Utility.showProgressDialog(context);
        apiServer.deleteLessonImage(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                list.remove(position);
                notifyDataSetChanged();
                Utility.dismissProgressDialog(pd);
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, id, image);*/
    }
}