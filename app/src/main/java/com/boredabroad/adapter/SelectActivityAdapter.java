package com.boredabroad.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boredabroad.R;
import com.boredabroad.model.MainActivityData;
import com.boredabroad.model.MenuData;
import com.boredabroad.model.SubActivityData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class SelectActivityAdapter extends BaseAdapter {
    Context context;
    ArrayList<SubActivityData> list;

    public SelectActivityAdapter(Context context, ArrayList<SubActivityData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_select_activity, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_title = view.findViewById(R.id.txt_title);
        holder.imgv_icon = view.findViewById(R.id.imgv_icon);
        holder.ll_divider = view.findViewById(R.id.ll_divider);
        holder.ll_main = view.findViewById(R.id.ll_main);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, final int position) {
        holder.txt_title.setText(list.get(position).getName());
        if(list.get(position).isSelected()){
            holder.txt_title.setTextColor(context.getResources().getColor(R.color.off_white));
            holder.imgv_icon.setImageResource(R.drawable.check_round);
            holder.imgv_icon.setColorFilter(context.getResources().getColor(R.color.font_saff));
        }else{
            holder.txt_title.setTextColor(context.getResources().getColor(R.color.white));
            holder.imgv_icon.setImageResource(R.drawable.round);
            holder.imgv_icon.setColorFilter(context.getResources().getColor(R.color.white));
        }

        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(list.get(position).isSelected()){
                    list.get(position).setSelected(false);
                }else {
                    list.get(position).setSelected(true);
                }
                notifyDataSetChanged();
            }
        });

        if(position==list.size()-1){
            holder.ll_divider.setVisibility(View.GONE);
        }else {
            holder.ll_divider.setVisibility(View.VISIBLE);
        }

    }


    private void initialize() {

    }

    private class Holder {
        TextView txt_title;
        ImageView imgv_icon;
        LinearLayout ll_divider,ll_main;
    }
}
