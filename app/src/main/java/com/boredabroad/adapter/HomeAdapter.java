package com.boredabroad.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.boredabroad.R;
import com.boredabroad.model.ProductData;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class HomeAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<ProductData> list;

    public HomeAdapter(Context context, ArrayList<ProductData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_all_trip, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_name = view.findViewById(R.id.txt_name);
    }

    @SuppressLint("NewApi")
    private void setData(final Holder holder, final int position) {

        holder.txt_name.setText(list.get(position).getName());


    }


    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    private class Holder {
        TextView txt_name;
    }
}
