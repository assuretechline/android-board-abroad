package com.boredabroad;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.ahmadrosid.svgloader.SvgLoader;
import com.boredabroad.adapter.CountryListAdapter;
import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.global.GlobalApplication;
import com.boredabroad.global.Utility;
import com.boredabroad.model.CountryData;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.internal.Util;


public class SignupActivity extends BaseActivity implements View.OnClickListener {

    Context context;
    TextView txt_submit, txt_sign_in;
    ImageView imgv_country, imgv_back, imgv_name, imgv_email, imgv_password, imgv_phone;
    EditText edt_name, edt_email, edt_password, edt_phone;
    ProgressDialog pd;
    APIServer apiServer;
    String countryId = "", countryName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Utility.setLoginStatusColor(this);
        initialize();
        setView();
        setData();
        setListener();
        setColor();
    }

    private void initialize() {
        context = this;
        apiServer = new APIServer(context);
    }

    private void setView() {
        txt_submit = findViewById(R.id.txt_submit);
        txt_sign_in = findViewById(R.id.txt_sign_in);
        imgv_back = findViewById(R.id.imgv_back);
        imgv_country = findViewById(R.id.imgv_country);
        imgv_name = findViewById(R.id.imgv_name);
        imgv_email = findViewById(R.id.imgv_email);
        imgv_password = findViewById(R.id.imgv_password);
        imgv_phone = findViewById(R.id.imgv_phone);
        edt_email = findViewById(R.id.edt_email);
        edt_name = findViewById(R.id.edt_name);
        edt_password = findViewById(R.id.edt_password);
        edt_phone = findViewById(R.id.edt_phone);
    }

    private void setData() {
        try {
            CountryData countryData = LoginActivity.currentCountry;
            SvgLoader.pluck().with((Activity) context).load(countryData.getImage(), imgv_country);
            countryId = countryData.getCode();
            countryName = countryData.getName();
        } catch (Exception e) {

        }
    }

    private void setListener() {
        txt_submit.setOnClickListener(this);
        txt_sign_in.setOnClickListener(this);
        imgv_back.setOnClickListener(this);
        imgv_country.setOnClickListener(this);
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_name.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_email.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_password.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_phone.setColorFilter(context.getResources().getColor(R.color.font_saff));
    }

    @Override
    public void onClick(View view) {
        if (view == txt_submit) {
            if (Utility.isNetworkAvailable(context)) {
                checkValidation();
            } else {
                Utility.errDialog(context.getString(R.string.network), context);
            }
        } else if (view == imgv_back) {
            Utility.gotoBack(context);
        } else if (view == txt_sign_in) {
            Utility.gotoBack(context);
        } else if (view == imgv_country) {
            openCountryDialog();
        }
    }

    private void checkValidation() {
        if (edt_name.getText().toString().equals("")) {
            Utility.errDialog("Please enter your name", context);
        } else if (edt_email.getText().toString().equals("")) {
            Utility.errDialog("Please enter Email Address", context);
        } else if (!isValidEmail(edt_email.getText())) {
            Utility.errDialog("Please enter valid Email Address", context);
        } else if (edt_password.getText().toString().equals("")) {
            Utility.errDialog("Please enter Password", context);
        } else if (edt_password.getText().length() < 8) {
            Utility.errDialog("Password must be at least 8 characters long", context);
        } else if (edt_phone.getText().toString().equals("")) {
            Utility.errDialog("Please enter Mobile Number", context);
        } else if (edt_phone.getText().length() < 9) {
            Utility.errDialog("Please enter valid Mobile Number", context);
        } else {
            Utility.hideSoftKeyboard(edt_password, context);
            signupCall();
        }
    }

    private void signupCall() {

        Log.e("Tag","Country code: "+countryId +" " +countryName);

        pd = Utility.showProgressDialog(context);
        apiServer.addUser(new APIResponse() {
                              @Override
                              public void onSuccess(JSONObject object) {
                                  try {
                                      //Log.e("Tag","data changes : "+object);
                                      if (object.getString("type").equals("success")) {
                                          final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                                          alertDialog2.setTitle("");
                                          alertDialog2.setCancelable(false);
                                          alertDialog2.setMessage(object.getString("message"));

                                          alertDialog2.setPositiveButton("Ok",
                                                  new DialogInterface.OnClickListener() {
                                                      public void onClick(DialogInterface dialog, int which) {
                                                          // Write your code here to execute after dialog
                                                          Utility.gotoBack(context);
                                                      }
                                                  });
                                          alertDialog2.show();
                                      } else {
                                          //Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                                          Utility.errDialog( object.getString("message"),context);
                                      }
                                      Utility.dismissProgressDialog(pd);
                                  } catch (JSONException e) {
                                      Utility.dismissProgressDialog(pd);
                                      e.printStackTrace();
                                  }
                              }

                              @Override
                              public void onFailure(String error) {
                                  Log.e("Tag","error changes : "+error);
                                  Utility.dismissProgressDialog(pd);
                                  Utility.errDialog(error, context);
                              }
                          }, edt_name.getText().toString().trim(),
                edt_email.getText().toString().trim(),
                edt_password.getText().toString().trim(),
                edt_phone.getText().toString().trim(),
                countryId + "",
                countryName+"");
    }

    public boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public void openCountryDialog() {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_country_dialog);

        final ImageView imgv_search = dialog.findViewById(R.id.imgv_search);
        final ImageView imgv_cancel = dialog.findViewById(R.id.imgv_cancel);
        TextView txt_cancel = dialog.findViewById(R.id.txt_cancel);
        final EditText edt_country = dialog.findViewById(R.id.edt_country);
        ListView lst_country = dialog.findViewById(R.id.lst_country);

        imgv_cancel.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_search.setColorFilter(context.getResources().getColor(R.color.white));

        final CountryListAdapter countryListAdapter = new CountryListAdapter(context, GlobalApplication.countryDataArrayList);
        lst_country.setAdapter(countryListAdapter);

        lst_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CountryData countryData = (CountryData) parent.getItemAtPosition(position);

              /*  Log.e("Tag", "Data " + countryData.getName());
                Log.e("Tag", "Data " + countryData.getImage());
                Log.e("Tag", "Data " + countryData.getCode());*/

                SvgLoader.pluck().with((Activity) context).load(countryData.getImage(), imgv_country);
                countryId = countryData.getCode();
                countryName = countryData.getName();
                dialog.dismiss();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        imgv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_country.setText("");
                imgv_cancel.setVisibility(View.GONE);
            }
        });

        edt_country.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                countryListAdapter.getFilter().filter(s);
                if (s.length() == 0) {
                    imgv_cancel.setVisibility(View.GONE);
                } else {
                    imgv_cancel.setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.show();
    }
}
