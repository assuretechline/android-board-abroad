package com.boredabroad.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.boredabroad.MainActivity;
import com.boredabroad.R;
import com.boredabroad.SearchLocationActivity;
import com.boredabroad.adapter.MonthAdapter;
import com.boredabroad.adapter.SelectActivityAdapter;
import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.global.MyPreference;
import com.boredabroad.global.Utility;
import com.boredabroad.model.MainActivityData;
import com.boredabroad.model.MenuData;
import com.boredabroad.model.SubActivityData;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;


public class AddTripFragment extends Fragment implements View.OnClickListener {
    View view, ll_feck_date;
    Context context;
    LinearLayout ll_footer, ll_holiday, ll_abroad, ll_date, ll_location, ll_interested, ll_show_actvity, ll_select_date, ll_month_view;
    FrameLayout ll_main;
    EditText edt_trip_title;
    ImageView imgv_ok, imgv_date, imgv_location, imgv_privacy, imgv_interested, imgv_previous, imgv_next, imgv_cancel,imgv_date_cancel, imgv_date_ok, imgv_holiday, imgv_abroad;
    ImageView imgv_date_info, imgv_location_info, imgv_privacy_user, imgv_interested_info, imgv_month_previous, imgv_month_next;
    TextView txt_month_year, txt_header, txt_date, txt_interested, txt_activity_title, txt_activity_count, txt_activity_done;
    public static TextView txt_location;
    int height;
    ArrayList<MainActivityData> activityDataArrayList = new ArrayList<>();
    SelectActivityAdapter selectActivityAdapter;
    ListView lst_activity;
    int selectedPosition = 0, month_view_year;
    PopupWindow mypopupWindow;
    RadioGroup rg_privacy;
    RadioButton rb_public, rb_private;
    MaterialCalendarView mtrl_cal;
    String newDate = "", selectPrivacy = "", selectType = "";
    boolean isUpdate;
    GridView gv_month;
    MonthAdapter monthAdapter;
    ArrayList<MenuData> monthArraylist = new ArrayList<>();
    ProgressDialog pd;
    APIServer apiServer;
    JSONObject jsonObjectActivity;
    public static String lat, lang, city, state, country;

    public AddTripFragment(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_add_trip, null, false);
        this.setHasOptionsMenu(true);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    private void initialize() {
        context = getContext();
        apiServer = new APIServer(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void setView(View view) {

        apiServer = new APIServer(context);
        txt_header = view.findViewById(R.id.txt_header);
        ll_footer = view.findViewById(R.id.ll_footer);
        ll_holiday = view.findViewById(R.id.ll_holiday);
        ll_abroad = view.findViewById(R.id.ll_abroad);
        ll_date = view.findViewById(R.id.ll_date);
        ll_location = view.findViewById(R.id.ll_location);
        ll_interested = view.findViewById(R.id.ll_interested);
        ll_show_actvity = view.findViewById(R.id.ll_show_actvity);
        ll_main = view.findViewById(R.id.ll_main);
        ll_select_date = view.findViewById(R.id.ll_select_date);
        edt_trip_title = view.findViewById(R.id.edt_trip_title);
        imgv_location = view.findViewById(R.id.imgv_location);
        imgv_date = view.findViewById(R.id.imgv_date);
        imgv_privacy = view.findViewById(R.id.imgv_privacy);
        imgv_interested = view.findViewById(R.id.imgv_interested);
        imgv_previous = view.findViewById(R.id.imgv_previous);
        imgv_next = view.findViewById(R.id.imgv_next);
        txt_activity_done = view.findViewById(R.id.txt_activity_done);
        imgv_date_ok = view.findViewById(R.id.imgv_date_ok);
        imgv_date_cancel = view.findViewById(R.id.imgv_date_cancel);
        imgv_abroad = view.findViewById(R.id.imgv_abroad);
        imgv_holiday = view.findViewById(R.id.imgv_holiday);
        imgv_ok = view.findViewById(R.id.imgv_ok);
        imgv_month_previous = view.findViewById(R.id.imgv_month_previous);
        imgv_month_next = view.findViewById(R.id.imgv_month_next);
        imgv_cancel = view.findViewById(R.id.imgv_cancel);
        txt_date = view.findViewById(R.id.txt_date);
        txt_activity_title = view.findViewById(R.id.txt_activity_title);
        txt_activity_count = view.findViewById(R.id.txt_activity_count);
        txt_location = view.findViewById(R.id.txt_location);
        txt_interested = view.findViewById(R.id.txt_interested);
        txt_month_year = view.findViewById(R.id.txt_month_year);
        lst_activity = view.findViewById(R.id.lst_activity);
        imgv_interested_info = view.findViewById(R.id.imgv_interested_info);
        imgv_date_info = view.findViewById(R.id.imgv_date_info);
        imgv_location_info = view.findViewById(R.id.imgv_location_info);
        imgv_privacy_user = view.findViewById(R.id.imgv_privacy_user);
        rb_private = view.findViewById(R.id.rb_private);
        rb_public = view.findViewById(R.id.rb_public);
        mtrl_cal = view.findViewById(R.id.mtrl_cal);
        gv_month = view.findViewById(R.id.gv_month);
        rg_privacy = view.findViewById(R.id.rg_privacy);
        ll_month_view = view.findViewById(R.id.ll_month_view);
        ll_feck_date = view.findViewById(R.id.ll_feck_date);
    }

    private void setData() {
        // Log.e("Tag", "pstr " + isUpdate);
        if (isUpdate) {
            txt_header.setText("Update Activites");
        } else {
            txt_header.setText("Add New");
        }

        monthArraylist.add(new MenuData("JAN", 1, false));
        monthArraylist.add(new MenuData("FAB", 2, false));
        monthArraylist.add(new MenuData("MAR", 3, false));
        monthArraylist.add(new MenuData("APR", 4, false));
        monthArraylist.add(new MenuData("MAY", 5, false));
        monthArraylist.add(new MenuData("JUN", 6, false));
        monthArraylist.add(new MenuData("JUL", 7, false));
        monthArraylist.add(new MenuData("AUG", 8, false));
        monthArraylist.add(new MenuData("SEP", 9, false));
        monthArraylist.add(new MenuData("OCT", 10, false));
        monthArraylist.add(new MenuData("NOV", 11, false));
        monthArraylist.add(new MenuData("DEC", 12, false));

        month_view_year = Integer.parseInt(Utility.getCurrentYear());
        txt_month_year.setText(month_view_year + "");
        imgv_month_previous.setAlpha(0.5f);

        mtrl_cal.setSelectedDate(CalendarDay.today());
        rb_public.setChecked(true);

        Calendar calendar = Calendar.getInstance();
        mtrl_cal.state().edit().setMinimumDate(calendar.getTimeInMillis()).commit();


        int month = calendar.getTime().getMonth();
        monthArraylist.get(month).setSelected(true);
        monthAdapter = new MonthAdapter(context, monthArraylist);
        gv_month.setAdapter(monthAdapter);

        selectPrivacy = "public";
        selectType = "holiday";

        if (Utility.isNetworkAvailable(context)) {
            getActivityList();
        } else {
            Utility.errDialog(context.getString(R.string.network), context);
        }
    }

    private void setListener() {
        ll_date.setOnClickListener(this);
        ll_location.setOnClickListener(this);
        ll_interested.setOnClickListener(this);
        imgv_next.setOnClickListener(this);
        imgv_previous.setOnClickListener(this);
        txt_activity_done.setOnClickListener(this);
        imgv_date_ok.setOnClickListener(this);
        imgv_date_cancel.setOnClickListener(this);
        imgv_date_info.setOnClickListener(this);
        imgv_location_info.setOnClickListener(this);
        imgv_privacy_user.setOnClickListener(this);
        imgv_interested_info.setOnClickListener(this);
        imgv_ok.setOnClickListener(this);
        ll_holiday.setOnClickListener(this);
        ll_abroad.setOnClickListener(this);
        imgv_month_next.setOnClickListener(this);
        imgv_month_previous.setOnClickListener(this);
        ll_feck_date.setOnClickListener(this);

        ll_main.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                ll_main.getWindowVisibleDisplayFrame(r);
                int screenHeight = ll_main.getRootView().getHeight();
                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    // keyboard is opened
                    ll_footer.setVisibility(View.GONE);
                } else {
                    // keyboard is closed
                    ll_footer.setVisibility(View.VISIBLE);
                }
            }
        });


        //newDate = mtrl_cal.getSelectedDate().getDay() + "/" + (mtrl_cal.getSelectedDate().getMonth() + 1) + "/" + mtrl_cal.getSelectedDate().getYear();
        mtrl_cal.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                newDate = date.getDay() + "/" + (date.getMonth() + 1) + "/" + date.getYear();
            }
        });


        rg_privacy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_public:
                        selectPrivacy = "public";
                        break;
                    case R.id.rb_private:
                        selectPrivacy = "private";
                        break;
                }
            }
        });
    }

    private void setColor() {
        imgv_holiday.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_abroad.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_location.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_date.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_interested.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_privacy.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_next.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_previous.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_date_ok.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_date_cancel.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_month_next.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_month_previous.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_ok.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_cancel.setColorFilter(context.getResources().getColor(R.color.white));
    }

    @Override
    public void onClick(View view) {
        if (view == ll_date) {
            Utility.expand(ll_select_date, height - getResources().getDimensionPixelSize(R.dimen._55sdp));
        } else if (view == ll_location) {
            Utility.gotoNext(context, SearchLocationActivity.class);
        } else if (view == ll_interested) {
            Utility.expand(ll_show_actvity, height - getResources().getDimensionPixelSize(R.dimen._55sdp));
        } else if (view == imgv_previous) {
            if (selectedPosition == 1) {
                selectedPosition = selectedPosition - 1;
                setActivityPosition(selectedPosition);
            } else if (selectedPosition == 0) {
            } else {
                selectedPosition = selectedPosition - 1;
                setActivityPosition(selectedPosition);
            }
        } else if (view == imgv_next) {
            if (selectedPosition == activityDataArrayList.size() - 2) {
                selectedPosition = selectedPosition + 1;
                setActivityPosition(selectedPosition);
            } else if (selectedPosition == activityDataArrayList.size() - 1) {
            } else {
                selectedPosition = selectedPosition + 1;
                setActivityPosition(selectedPosition);
            }
        } else if (view == ll_feck_date) {
            Utility.collapse(ll_select_date);
        } else if (view == txt_activity_done) {
            activityViewClose();
        } else if (view == imgv_date_cancel) {
            Utility.collapse(ll_select_date);
        } else if (view == imgv_date_ok) {
            Utility.collapse(ll_select_date);
            if (selectType.equals("holiday")) {
                if (!newDate.equals("")) {
                    txt_date.setText(newDate);
                }
            } else {
                String selectMonth = "";
                for (int i = 0; i < monthArraylist.size(); i++) {
                    if (monthArraylist.get(i).isSelected()) {
                        if (i < 9) {
                            selectMonth = "0" + (i + 1);
                        } else {
                            selectMonth = "" + (i + 1);
                        }
                    }
                }
                txt_date.setText(selectMonth + "/" + txt_month_year.getText().toString());
            }

        } else if (view == imgv_date_info) {
            setPopUpWindow("Info will display here");
            mypopupWindow.showAsDropDown(imgv_date_info, 0, 10);
        } else if (view == imgv_location_info) {
            setPopUpWindow("Info will display here");
            mypopupWindow.showAsDropDown(imgv_location_info, 0, 10);
        } else if (view == imgv_privacy_user) {
            setPopUpWindow("Info will display here");
            mypopupWindow.showAsDropDown(imgv_privacy_user, 0, 10);
        } else if (view == imgv_interested_info) {
            setPopUpWindow("Info will display here");
            mypopupWindow.showAsDropDown(imgv_interested_info, 0, 10);
        } else if (view == imgv_ok) {
            //MainActivity.getInstance().setFragmentMainTab(new MapFragment(), "MapFragment");
            checkValidation();
        } else if (view == ll_abroad) {
            selectType = "abroad";
            imgv_abroad.setImageResource(R.drawable.selected);
            imgv_holiday.setImageResource(R.drawable.unselected);
            if (ll_month_view.getVisibility() != View.VISIBLE) {
                mtrl_cal.setVisibility(View.GONE);
                ll_month_view.setVisibility(View.VISIBLE);
            }
        } else if (view == ll_holiday) {
            selectType = "holiday";
            imgv_abroad.setImageResource(R.drawable.unselected);
            imgv_holiday.setImageResource(R.drawable.selected);
            if (mtrl_cal.getVisibility() != View.VISIBLE) {
                ll_month_view.setVisibility(View.GONE);
                mtrl_cal.setVisibility(View.VISIBLE);
            }
        } else if (view == imgv_month_previous) {
            if (!txt_month_year.getText().toString().equals(Utility.getCurrentYear())) {
                month_view_year = month_view_year - 1;
                txt_month_year.setText(month_view_year + "");
            }

            if (txt_month_year.getText().toString().equals(Utility.getCurrentYear())) {
                imgv_month_previous.setAlpha(0.5f);
            }

        } else if (view == imgv_month_next) {
            imgv_month_previous.setAlpha(1f);
            month_view_year = month_view_year + 1;
            txt_month_year.setText(month_view_year + "");
        }
    }

    private void activityViewClose() {
        Utility.collapse(ll_show_actvity);
        String name = "";
        jsonObjectActivity = new JSONObject();

        ArrayList<SubActivityData> subActivityDataArrayList = new ArrayList<>();
        for (int i = 0; i < activityDataArrayList.size(); i++) {
            subActivityDataArrayList.clear();
            subActivityDataArrayList.addAll(activityDataArrayList.get(i).getSubActivityDataArrayList());
            JSONArray jsonArr = new JSONArray();
            for (int j = 0; j < subActivityDataArrayList.size(); j++) {
                if (subActivityDataArrayList.get(j).isSelected()) {
                    jsonArr.put(Integer.parseInt(subActivityDataArrayList.get(j).getId()));
                    if (name.equals("")) {
                        name += "" + subActivityDataArrayList.get(j).getName();
                    } else {
                        name += ", " + subActivityDataArrayList.get(j).getName();
                    }
                }
            }
            if (jsonArr.length() > 0) {
                try {
                    jsonObjectActivity.put(activityDataArrayList.get(i).getId(), jsonArr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        txt_interested.setText(name);
       // Log.e("Tag", "jsonobject : " + jsonObjectActivity);
    }

    private void setActivityPosition(int position) {
        txt_activity_count.setText((position + 1) + " / " + activityDataArrayList.size());
        txt_activity_title.setText(activityDataArrayList.get(position).getName());
        selectActivityAdapter = new SelectActivityAdapter(context, activityDataArrayList.get(position).getSubActivityDataArrayList());
        lst_activity.setAdapter(selectActivityAdapter);

        imgv_next.setAlpha(1f);
        imgv_previous.setAlpha(1f);

        if (selectedPosition == 0) {
            imgv_previous.setAlpha(0.5f);
        }

        if (selectedPosition == activityDataArrayList.size() - 1) {
            imgv_next.setAlpha(0.5f);
        }
    }

    private void setPopUpWindow(String text) {
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View myView = inflater.inflate(R.layout.popup_view, null);
        TextView txt_msg = myView.findViewById(R.id.txt_msg);
        txt_msg.setText(text);
        mypopupWindow = new PopupWindow(myView, context.getResources().getDimensionPixelOffset(R.dimen._155sdp), RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        mypopupWindow.setOutsideTouchable(true);
        mypopupWindow.setFocusable(true);
        mypopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void getActivityList() {
        pd = Utility.showProgressDialog(context);
        apiServer.getActivity(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    //Log.e("Tag", "data changes : " + object);
                    if (object.getString("type").equals("success")) {
                        JSONArray jsonArray = object.getJSONArray("data");
                        ArrayList<SubActivityData> subActivityData;
                        for (int j = 0; j < jsonArray.length(); j++) {
                            subActivityData = new ArrayList<>();
                            JSONArray jsonArrayChild = jsonArray.getJSONObject(j).getJSONArray("child");
                            for (int i = 0; i < jsonArrayChild.length(); i++) {
                                subActivityData.add(new SubActivityData(jsonArrayChild.getJSONObject(i).getString("id"), jsonArrayChild.getJSONObject(i).getString("name"), false));
                            }
                            activityDataArrayList.add(new MainActivityData(jsonArray.getJSONObject(j).getString("id"), jsonArray.getJSONObject(j).getString("name"), subActivityData));
                        }
                        setActivityPosition(selectedPosition);
                    } else {
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissProgressDialog(pd);
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        });
    }

    private void checkValidation() {
        if (edt_trip_title.getText().toString().isEmpty()) {
            Utility.errDialog("Please enter Trip Title", context);
        } else if (txt_date.getText().toString().isEmpty()) {
            Utility.errDialog("Please select Date", context);
        } else if (txt_location.getText().toString().isEmpty()) {
            Utility.errDialog("Please select Location", context);
        } else if (txt_interested.getText().toString().isEmpty()) {
            Utility.errDialog("Please select Interest", context);
        } else {
            addTrip();
        }

    }

    private void addTrip() {
        pd = Utility.showProgressDialog(context);
        apiServer.addTrip(new APIResponse() {
                              @Override
                              public void onSuccess(JSONObject object) {
                                  try {
                                      Log.e("Tag", "response addTrip : " + object);
                                      if (object.getString("type").equals("success")) {

                                          edt_trip_title.setText("");
                                          newDate = "";
                                          txt_interested.setText("");
                                          txt_date.setText("");
                                          txt_location.setText("");
                                          final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                                          alertDialog2.setTitle("Success");
                                          alertDialog2.setCancelable(false);
                                          alertDialog2.setMessage(object.getString("message"));
                                          alertDialog2.setPositiveButton("Ok",
                                                  new DialogInterface.OnClickListener() {
                                                      public void onClick(DialogInterface dialog, int which) {
                                                          dialog.cancel();
                                                          ((MainActivity)getActivity()).handleMenuClick(0);
                                                      }
                                                  });

                                          alertDialog2.show();
                                      } else {
                                          Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                                      }
                                      Utility.dismissProgressDialog(pd);
                                  } catch (JSONException e) {
                                      Utility.dismissProgressDialog(pd);
                                      e.printStackTrace();
                                  }
                              }

                              @Override
                              public void onFailure(String error) {
                                  Utility.dismissProgressDialog(pd);
                                  Utility.errDialog(context.getString(R.string.api_failure), context);
                              }
                          },
                edt_trip_title.getText().toString(),
                txt_date.getText().toString(),
                selectType,
                lat,
                lang,
                selectPrivacy,
                jsonObjectActivity.toString(),
                city,
                state,
                country,
                MyPreference.getPreferenceValue(context, "id").toString()
        );
    }

}
