package com.boredabroad.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.boredabroad.LoginActivity;
import com.boredabroad.R;
import com.boredabroad.adapter.AddImageListingAdapter;
import com.boredabroad.adapter.CountryListAdapter;
import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.global.GlobalApplication;
import com.boredabroad.global.MyPreference;
import com.boredabroad.global.Utility;
import com.boredabroad.model.AddImageListingData;
import com.boredabroad.model.CountryData;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.OAuthProvider;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by home on 14/07/18.
 */


public class SettingFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    EditText edt_name, edt_email, edt_password, edt_phone;
    ImageView imgv_edit, imgv_name, imgv_phone, imgv_email, imgv_password, imgv_camera, imgv_country, imgv_next_twitter, imgv_next_insta, imgv_next_fb, imgv_logout;
    CircleImageView imgv_profile;
    boolean isEdit = false;
    ArrayList<File> fileListArrayList = new ArrayList<>();
    public static int RESULT_CODE = 102, PICKFILE_REQUEST_CODE = 1003;
    ProgressDialog pd;
    APIServer apiServer;
    RecyclerView rv_image;
    ArrayList<AddImageListingData> addImageListingDataArrayList = new ArrayList<>();
    AddImageListingAdapter addImageListingAdapter;
    LinearLayout ll_twitter, ll_instagram, ll_fb;
    TextView txt_country, txt_twitter, txt_insta, txt_facebook;
    private TwitterLoginButton loginButton;
    OAuthProvider.Builder provider;
    LoginButton fb_login_button;
    CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    String countryId = "", countryName = "";

    @SuppressLint("ValidFragment")
    public SettingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(R.layout.fragment_setting, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
        if (!Utility.verifyStoragePermissions(context)) {
            requestForCameraPermission();
        }
        if (Utility.isNetworkAvailable(context)) {
            pd = Utility.showProgressDialog(context);
            getUserDetail();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
    }

    private void setView(View view) {
        edt_name = view.findViewById(R.id.edt_name);
        edt_email = view.findViewById(R.id.edt_email);
        edt_password = view.findViewById(R.id.edt_password);
        edt_phone = view.findViewById(R.id.edt_phone);
        txt_country = view.findViewById(R.id.txt_country);
        imgv_phone = view.findViewById(R.id.imgv_phone);
        imgv_name = view.findViewById(R.id.imgv_name);
        imgv_edit = view.findViewById(R.id.imgv_edit);
        imgv_email = view.findViewById(R.id.imgv_email);
        imgv_password = view.findViewById(R.id.imgv_password);
        imgv_camera = view.findViewById(R.id.imgv_camera);
        imgv_logout = view.findViewById(R.id.imgv_logout);
        imgv_next_twitter = view.findViewById(R.id.imgv_next_twitter);
        imgv_country = view.findViewById(R.id.imgv_country);
        imgv_next_insta = view.findViewById(R.id.imgv_next_insta);
        imgv_next_fb = view.findViewById(R.id.imgv_next_fb);
        imgv_profile = view.findViewById(R.id.imgv_profile);
        rv_image = view.findViewById(R.id.rv_image);

        ll_fb = view.findViewById(R.id.ll_fb);
        ll_instagram = view.findViewById(R.id.ll_instagram);
        ll_twitter = view.findViewById(R.id.ll_twitter);

        txt_facebook = view.findViewById(R.id.txt_facebook);
        txt_insta = view.findViewById(R.id.txt_insta);
        txt_twitter = view.findViewById(R.id.txt_twitter);
        txt_country = view.findViewById(R.id.txt_country);

        loginButton = (TwitterLoginButton) view.findViewById(R.id.loginButton);
        fb_login_button = (LoginButton) view.findViewById(R.id.fb_login_button);
    }

    private void setData() {
        mAuth = FirebaseAuth.getInstance();
        loginTwitterSetup();

        addImageListingAdapter = new AddImageListingAdapter(context, addImageListingDataArrayList, this);
        rv_image.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rv_image.setAdapter(addImageListingAdapter);

        try {
            edt_name.setText(MyPreference.getPreferenceValue(context, "name").toString());
            txt_country.setText(MyPreference.getPreferenceValue(context, "countryName").toString());
            edt_email.setText(MyPreference.getPreferenceValue(context, "email").toString());
            edt_phone.setText(MyPreference.getPreferenceValue(context, "mobile").toString());
            edt_password.setText("--------");
        } catch (Exception e) {

        }


        try {
            JSONObject jsonObject = new JSONObject(MyPreference.getPreferenceValue(context, "mainData").toString());
            JSONArray imageArray = jsonObject.getJSONArray("image");
            if (imageArray.length() > 0) {
                addImageListingDataArrayList.clear();
                for (int i = 0; i < imageArray.length(); i++) {
                    if (i == 0) {
                        ImageLoader.getInstance().displayImage(imageArray.getJSONObject(0).getString("thumb"), imgv_profile, Utility.getUserProfileOption());
                        addImageListingDataArrayList.add(new AddImageListingData(imageArray.getJSONObject(i).getString("thumb"), imageArray.getJSONObject(i).getString("orignal"), "", false));
                    } else {
                        addImageListingDataArrayList.add(new AddImageListingData(imageArray.getJSONObject(i).getString("thumb"), imageArray.getJSONObject(i).getString("orignal"), "", false));
                    }
                }
                addImageListingAdapter.notifyDataSetChanged();
            } else {
                ImageLoader.getInstance().displayImage("", imgv_profile, Utility.getUserProfileOption());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setDisable();

        //Twitter Login
        loginButton.setCallback(new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> result) {
                pd = Utility.showProgressDialog(context);
                Log.e("Tag", "loginButton Callback: Success");
                //Log.e("Tag", "loginButton Callback: Success" +result.response.g);
                String oauth_token_secret = result.data.getAuthToken().secret;
                String oauth_token = result.data.getAuthToken().token;
                TwitterSession user = result.data;
                getTwitterUserDetails(user);

            }

            @Override
            public void failure(TwitterException exception) {
                Log.e("Tag", "loginButton Callback: Failure " +
                        exception.getLocalizedMessage());
            }
        });


        //FB Login
        callbackManager = CallbackManager.Factory.create();
        //fb_login_button.setReadPermissions("email", "public_profile", "link");
        fb_login_button.setReadPermissions("email", "public_profile");
        fb_login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("Tag", "FB LOGIN loginResult  : " + loginResult);
                // App code
                //handleFacebookAccessToken(loginResult.getAccessToken());
                Log.e("Tag", "FB LOGIN getAccessToken  : " + loginResult.getAccessToken());
                MyPreference.setPreferenceValue(context, "tokenFB", loginResult.getAccessToken().getUserId());
                String profilePicUrl = "https://graph.facebook.com/" + loginResult.getAccessToken().getUserId() + "/picture?type=large";
                MyPreference.setPreferenceValue(context, "facebookProfileImage", profilePicUrl);
                Log.e("Tag", "FB LOGIN profilePicUrl  : " + profilePicUrl);
                //Utility.dismissProgressDialog(pd);
                //Utility.successDialog("Facebook connected to your profile", context);
                addSocial("facebook", loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                // App code
                Log.e("Tag", "FB LOGIN  onCancel ");
                Utility.errDialog("Facebook Login Is Cancelled By User", context);
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("Tag", "FB LOGIN  exception : " + exception.getMessage());
                Toast.makeText(getContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setListener() {
        edt_name.setOnClickListener(this);
        edt_email.setOnClickListener(this);
        imgv_logout.setOnClickListener(this);
        imgv_camera.setOnClickListener(this);
        imgv_profile.setOnClickListener(this);
        imgv_edit.setOnClickListener(this);
        txt_country.setOnClickListener(this);
        ll_fb.setOnClickListener(this);
        ll_instagram.setOnClickListener(this);
        ll_twitter.setOnClickListener(this);
    }

    private void setColor() {
        imgv_edit.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_phone.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_name.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_password.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_email.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_country.setColorFilter(context.getResources().getColor(R.color.font_saff));
        imgv_logout.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_camera.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_next_twitter.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_next_insta.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_next_fb.setColorFilter(context.getResources().getColor(R.color.white));
    }

    @Override
    public void onClick(View view) {
        if (view == edt_name) {

        } else if (view == edt_email) {

        } else if (view == imgv_camera) {
            imageFromGallery();
        } else if (view == imgv_logout) {
            setLogout();
        } else if (view == imgv_edit) {
            if (isEdit) {
                isEdit = false;
                imgv_edit.setImageResource(R.drawable.pen);
                setDisable();
                updateUserApiCall();
            } else {
                isEdit = true;
                imgv_edit.setImageResource(R.drawable.tick);
                setEnable();
            }
        } else if (view == ll_fb) {
            if (isEdit) {
                if (txt_facebook.getText().toString().equals("Connect")) {
                    LoginManager.getInstance().logOut();
                    fb_login_button.performClick();
                } else {
                    removeSocial("facebook");
                }
            }
        } else if (view == ll_instagram) {
            if (isEdit) {
                if (txt_insta.getText().toString().equals("Connect")) {
                    customAddInstaWS("instagram");
                } else {
                    removeSocial("instagram");
                }
            }
        } else if (view == ll_twitter) {
            if (isEdit) {
                if (txt_twitter.getText().toString().equals("Connect")) {
                    loginButton.performClick();
                } else {
                    removeSocial("twitter");
                }
            }
        } else if (view == txt_country) {
            if (isEdit)
                openCountryDialog();
        }
    }

    public void getTwitterUserDetails(TwitterSession twitterSession) {

        TwitterCore.getInstance().getApiClient(twitterSession).getAccountService().verifyCredentials(true, true, false).enqueue(new Callback<User>() {
            @Override
            public void success(Result<User> userResult) {
                try {
                    User user = userResult.data;
                    long twitterID = user.getId();
                    String userScreenName = user.screenName;
                    MyPreference.setPreferenceValue(context, "twitterID", twitterID + "");
                    MyPreference.setPreferenceValue(context, "twitterUserScreenName", userScreenName);
                    addSocial("twitter", twitterID + "");
                    //Utility.dismissProgressDialog(pd);
                    //Utility.successDialog("Twitter connected to your profile", context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(TwitterException e) {
            }
        });
    }

    private void setEnable() {
        rv_image.setVisibility(View.VISIBLE);
        edt_name.setEnabled(true);
        edt_email.setEnabled(true);
        edt_password.setEnabled(true);
        edt_phone.setEnabled(true);
        txt_country.setEnabled(true);
    }

    private void setDisable() {
        rv_image.setVisibility(View.GONE);
        edt_name.setEnabled(false);
        edt_email.setEnabled(false);
        edt_password.setEnabled(false);
        edt_phone.setEnabled(false);
        txt_country.setEnabled(false);
    }

    private void updateUserApiCall() {
        String pass = "";
        if (edt_password.getText().toString().equals("--------")) {
            pass = "";
        } else {
            pass = edt_password.getText().toString();
        }

        pd = Utility.showProgressDialog(context);
        apiServer.updateUser(new APIResponse() {
                                 @Override
                                 public void onSuccess(JSONObject object) {
                                     try {
                                         //Log.e("Tag","data changes : "+object);
                                         if (object.getString("type").equals("success")) {

                                             MyPreference.setPreferenceValue(context, "name", edt_name.getText().toString());
                                             MyPreference.setPreferenceValue(context, "email", edt_email.getText().toString());
                                             MyPreference.setPreferenceValue(context, "mobile", edt_phone.getText().toString());
                                             MyPreference.setPreferenceValue(context, "country", countryId);
                                             MyPreference.setPreferenceValue(context, "countryName", countryName);
                                             MyPreference.setPreferenceValue(context, "mainData", object.getJSONObject("data").toString());

                                             final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                                             alertDialog2.setTitle("");
                                             alertDialog2.setCancelable(false);
                                             alertDialog2.setMessage(object.getString("message"));

                                             alertDialog2.setPositiveButton("Ok",
                                                     new DialogInterface.OnClickListener() {
                                                         public void onClick(DialogInterface dialog, int which) {
                                                             setDisable();
                                                         }
                                                     });
                                             alertDialog2.show();

                                         } else {
                                             Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                                         }
                                         Utility.dismissProgressDialog(pd);
                                     } catch (JSONException e) {
                                         Utility.dismissProgressDialog(pd);
                                         e.printStackTrace();
                                     }
                                 }

                                 @Override
                                 public void onFailure(String error) {
                                     Utility.dismissProgressDialog(pd);
                                     Utility.errDialog(context.getString(R.string.api_failure), context);
                                 }
                             },
                edt_name.getText().toString().trim(),
                edt_email.getText().toString().trim(),
                pass,
                edt_phone.getText().toString().trim(),
                MyPreference.getPreferenceValue(context, "id").toString(),
                countryName + "",
                countryId
        );

    }

    public void setLogout() {
        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
        alertDialog2.setTitle("Logout");
        alertDialog2.setMessage("Are you sure you want to Logout?");
        alertDialog2.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent i = new Intent(context, LoginActivity.class);
                        context.startActivity(i);
                        ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                        getActivity().finish();
                    }
                });

        alertDialog2.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog2.show();
    }

    private void addUserProfileImage(File path) {
        pd = Utility.showProgressDialog(context);

        AndroidNetworking.upload("http://abroad.hiveschools.com/api/user/profile")
                .addHeaders("Auth-Key", GlobalApplication.auth)
                .addHeaders("Client-Service", GlobalApplication.client)
                .addMultipartFile("url[]", path)
                .addMultipartParameter("userid", MyPreference.getPreferenceValue(context, "id").toString())
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Tag", "Profile Up[load :  " + response);
                        try {
                            if (response.getString("type").equals("success")) {
                                getUserDetail();
                            } else {
                                Utility.errDialog(response.getString("message"), context);
                            }
                            Utility.dismissProgressDialog(pd);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(context.getString(R.string.api_failure), context);
                    }
                });


        /*apiServer.userProfileImage(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag", "response addTrip : " + object);
                    if (object.getString("type").equals("success")) {

                    } else {
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissProgressDialog(pd);
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, "", MyPreference.getPreferenceValue(context, "id").toString());*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //Log.e("Tag", data + " requestCode code : " + requestCode);
        //Log.e("Tag", data + " resultCode code : " + resultCode);
        if (data == null) {
            //Log.e("Tag", "Null Data");
        }

        if (resultCode == RESULT_CODE) {
            //finish();
        } else if (requestCode == 1005 && data != null) {
            addview(data, requestCode);
        } else if (requestCode == PICKFILE_REQUEST_CODE && data != null) {
            addview(data, requestCode);
        } else {
            //Toast.makeText(context, "No item selected", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }

    @SuppressLint("NewApi")
    private void addview(final Intent data, final int requestCode) {
        try {
            if (requestCode == 1005) {
                Log.e("Tag", "Gallary");
                Uri uri = data.getData();
                //Log.e("Tag","RESULT_GALLARY Utility : "+uri.toString());
                String path = null;
                File f = null;
                try {
                    path = Utility.getRealPathFromURI(context, uri);
                    f = new File(path);
                    //ImageLoader.getInstance().displayImage("file://" + f, imgv_feature, Utility.getImageOptions());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                if (path != null) {
                    addImageListingDataArrayList.add(0, new AddImageListingData(
                            "",
                            "" + path,
                            "file://" + f,
                            false));
                    addUserProfileImage(f);
                }

                //addImageListingAdapter.notifyDataSetChanged();
            } else if (requestCode == PICKFILE_REQUEST_CODE) {
                Log.e("Tag", "Gallary " + data.getData());
                Uri uri = data.getData();
                fileListArrayList = new ArrayList<>();
                try {
                    String path = Utility.getRealPathFromURI(context, uri);
                    if (path != null) {
                        try {
                            fileListArrayList.add(new File(path));
                            Log.e("Tag", "Data path  : " + path);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                }
            }

            //Log.e("Tag","File List : "+fileListArrayList.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 110://READ_EXTERNAL_STORAGE
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                break;
            case 120: //CAMERA
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void loginTwitterSetup() {
        TwitterAuthConfig mTwitterAuthConfig = new TwitterAuthConfig(getString(R.string.twitter_consumer_key), getString(R.string.twitter_consumer_secret));
        TwitterConfig twitterConfig = new TwitterConfig.Builder(context)
                .twitterAuthConfig(mTwitterAuthConfig)
                .build();
        Twitter.initialize(twitterConfig);

        provider = OAuthProvider.newBuilder("twitter.com");
        Task<AuthResult> pendingResultTask = mAuth.getPendingAuthResult();
        if (pendingResultTask != null) {
            // There's something already here! Finish the sign-in for your user.
            pendingResultTask
                    .addOnSuccessListener(
                            new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                }
                            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                        }
                    });
        } else {
        }
    }

    private void requestForCameraPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 120);
    }

    private void imageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1005);
    }

    private void getUserDetail() {
        apiServer.getUserDetail(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag", "getUserDetail : " + object);
                    if (object.getString("type").equals("success")) {
                        JSONArray imageArray = object.getJSONObject("data").getJSONArray("image");

                        MyPreference.setPreferenceValue(context, "id", object.getJSONObject("data").getString("id"));
                        MyPreference.setPreferenceValue(context, "name", object.getJSONObject("data").getString("name"));
                        MyPreference.setPreferenceValue(context, "email", object.getJSONObject("data").getString("email"));
                        MyPreference.setPreferenceValue(context, "mobile", object.getJSONObject("data").getString("mobile"));
                        MyPreference.setPreferenceValue(context, "country", object.getJSONObject("data").getString("code"));
                        MyPreference.setPreferenceValue(context, "countryName", object.getJSONObject("data").getString("country"));
                        MyPreference.setPreferenceValue(context, "mainData", object.getJSONObject("data").toString());

                        countryId = object.getJSONObject("data").getString("code");
                        countryName = object.getJSONObject("data").getString("country");

                        Log.e("Tag", "Dat : " + object.getJSONObject("data").getString("country"));

                        edt_name.setText(MyPreference.getPreferenceValue(context, "name").toString());
                        txt_country.setText(MyPreference.getPreferenceValue(context, "countryName").toString());
                        edt_email.setText(MyPreference.getPreferenceValue(context, "email").toString());
                        edt_phone.setText(MyPreference.getPreferenceValue(context, "mobile").toString());

                        if (imageArray.length() > 0) {
                            addImageListingDataArrayList.clear();
                            for (int i = 0; i < imageArray.length(); i++) {
                                if (i == 0) {
                                    ImageLoader.getInstance().displayImage(imageArray.getJSONObject(0).getString("thumb"), imgv_profile, Utility.getUserProfileOption());
                                    addImageListingDataArrayList.add(new AddImageListingData(imageArray.getJSONObject(i).getString("thumb"), imageArray.getJSONObject(i).getString("orignal"), "", false));
                                } else {
                                    addImageListingDataArrayList.add(new AddImageListingData(imageArray.getJSONObject(i).getString("thumb"), imageArray.getJSONObject(i).getString("orignal"), "", false));
                                }
                            }
                            addImageListingAdapter.notifyDataSetChanged();
                        } else {
                            ImageLoader.getInstance().displayImage("", imgv_profile, Utility.getUserProfileOption());
                        }

                        JSONObject socialObject = object.getJSONObject("data").getJSONObject("social");
                        if (!socialObject.getString("facebook").equals("")) {
                            txt_facebook.setText("Connected");
                        } else {
                            txt_facebook.setText("Connect");
                        }
                        if (!socialObject.getString("instagram").equals("")) {
                            txt_insta.setText("Connected");
                        } else {
                            txt_insta.setText("Connect");
                        }
                        if (!socialObject.getString("twitter").equals("")) {
                            txt_twitter.setText("Connected");
                        } else {
                            txt_twitter.setText("Connect");
                        }

                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, MyPreference.getPreferenceValue(context, "id").toString());
    }

    public void deleteImage(int position) {
        deleteImageAPI(position);
    }

    private void deleteImageAPI(int position) {
        pd = Utility.showProgressDialog(context);

        // Log.e("Tag","Delete : "+ addImageListingDataArrayList.get(position).getId());

        apiServer.deleteProfileUser(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag", "deleteProfileUser : " + object);
                    if (object.getString("type").equals("success")) {
                        getUserDetail();
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, addImageListingDataArrayList.get(position).getImgPath(), MyPreference.getPreferenceValue(context, "id").toString());
    }

    public void customAddInstaWS(final String type) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_add_insta_ws_dialog);

        final ImageView imgv_icon = dialog.findViewById(R.id.imgv_icon);
        TextView txt_title = dialog.findViewById(R.id.txt_title);
        TextView txt_submit = dialog.findViewById(R.id.txt_submit);
        final EditText edt_id = dialog.findViewById(R.id.edt_id);

        if (type.equals("instagram")) {
            imgv_icon.setImageResource(R.drawable.insta);
            edt_id.setHint("Enter your instagram id");
        } else {
            imgv_icon.setImageResource(R.drawable.ws);
            edt_id.setHint("Enter your mobile number");
        }

        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_id.getText().toString().equals("")) {
                    if (type.equals("instagram")) {
                        Utility.errDialog("Please enter your instagram id", context);
                    } else {
                        Utility.errDialog("Please enter your mobile number", context);
                    }
                } else {
                    pd = Utility.showProgressDialog(context);
                    addSocial(type, edt_id.getText().toString());
                }
            }
        });
        dialog.show();
    }

    private void addSocial(final String key, String value) {
        //pd = Utility.showProgressDialog(context);
        apiServer.addSocial(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    //Log.e("Tag","data changes : "+object);
                    if (object.getString("type").equals("success")) {

                        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                        alertDialog2.setTitle("");
                        alertDialog2.setCancelable(false);
                        alertDialog2.setMessage(object.getString("message"));

                        alertDialog2.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        getUserDetail();
                                    }
                                });
                        alertDialog2.show();
                    } else {
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissProgressDialog(pd);
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, MyPreference.getPreferenceValue(context, "id").toString(), key, value);
    }

    private void removeSocial(final String key) {
        pd = Utility.showProgressDialog(context);
        apiServer.removeSocial(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag", "data changes remove Social : " + object);
                    if (object.getString("type").equals("success")) {

                        try {
                            if (key.equals("facebook")) {
                                LoginManager.getInstance().logOut();
                            }
                        } catch (Exception e) {

                        }


                        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                        alertDialog2.setTitle("");
                        alertDialog2.setCancelable(false);
                        alertDialog2.setMessage(object.getString("message"));
                        alertDialog2.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        getUserDetail();
                                    }
                                });
                        alertDialog2.show();
                    } else {
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissProgressDialog(pd);
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(error, context);
            }
        }, MyPreference.getPreferenceValue(context, "id").toString(), key);
    }

    public void openCountryDialog() {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_country_dialog);

        final ImageView imgv_search = dialog.findViewById(R.id.imgv_search);
        final ImageView imgv_cancel = dialog.findViewById(R.id.imgv_cancel);
        TextView txt_cancel = dialog.findViewById(R.id.txt_cancel);
        final EditText edt_country = dialog.findViewById(R.id.edt_country);
        ListView lst_country = dialog.findViewById(R.id.lst_country);

        imgv_cancel.setColorFilter(context.getResources().getColor(R.color.white));
        imgv_search.setColorFilter(context.getResources().getColor(R.color.white));

        final CountryListAdapter countryListAdapter = new CountryListAdapter(context, GlobalApplication.countryDataArrayList);
        lst_country.setAdapter(countryListAdapter);

        lst_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CountryData countryData = (CountryData) parent.getItemAtPosition(position);

                /*Log.e("Tag", "Data " + countryData.getName());
                Log.e("Tag", "Data " + countryData.getImage());
                Log.e("Tag", "Data " + countryData.getCode());*/

                txt_country.setText(countryData.getName());
                countryId = countryData.getCode();
                countryName = countryData.getName();
                dialog.dismiss();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        imgv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_country.setText("");
                imgv_cancel.setVisibility(View.GONE);
            }
        });

        edt_country.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                countryListAdapter.getFilter().filter(s);
                if (s.length() == 0) {
                    imgv_cancel.setVisibility(View.GONE);
                } else {
                    imgv_cancel.setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.show();
    }

}