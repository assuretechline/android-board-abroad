package com.boredabroad.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.boredabroad.MainActivity;
import com.boredabroad.R;
import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.global.MyPreference;
import com.boredabroad.global.Utility;
import com.boredabroad.widget.AuthenticationListener;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.OAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;


public class SocialFragment extends Fragment implements View.OnClickListener, AuthenticationListener {
    View view;
    Context context;
    FrameLayout ll_fb, ll_insta, ll_ws, ll_twitter;
    TextView txt_next, txt_all_connect;
    ImageView imgv_info;
    PopupWindow mypopupWindow;
    boolean isNextVisible;
    JSONObject jsonObject;
    private FirebaseAuth mAuth;
    private TwitterLoginButton loginButton;
    OAuthProvider.Builder provider;
    ProgressDialog pd;
    LoginButton fb_login_button;
    CallbackManager callbackManager;
    APIServer apiServer;

    public SocialFragment(boolean isNextVisible) {
        this.isNextVisible = isNextVisible;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FacebookSdk.sdkInitialize(getContext());
        view = inflater.inflate(R.layout.fragment_social, null, false);
        this.setHasOptionsMenu(true);
        setView(view);
        setData();
        setListener();
        setColor();
        if (Utility.isNetworkAvailable(context)) {
            getUserDetail();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void setView(View view) {
        context = getContext();
        apiServer = new APIServer(context);

        txt_all_connect = view.findViewById(R.id.txt_all_connect);
        ll_fb = view.findViewById(R.id.ll_fb);
        ll_insta = view.findViewById(R.id.ll_insta);
        ll_ws = view.findViewById(R.id.ll_ws);
        ll_twitter = view.findViewById(R.id.ll_twitter);
        txt_next = view.findViewById(R.id.txt_next);
        imgv_info = view.findViewById(R.id.imgv_info);
        loginButton = (TwitterLoginButton) view.findViewById(R.id.loginButton);
        fb_login_button = (LoginButton) view.findViewById(R.id.fb_login_button);
    }

    private void setData() {
        mAuth = FirebaseAuth.getInstance();
        loginTwitterSetup();

        if (isNextVisible) {
            txt_next.setVisibility(View.VISIBLE);
        } else {
            txt_next.setVisibility(View.GONE);
        }

        try {
            if (MyPreference.getPreferenceValue(context, "mainData") != null) {
                jsonObject = new JSONObject(MyPreference.getPreferenceValue(context, "mainData").toString());
                //Log.e("Tag", "jsonObject : " + jsonObject);
            } else {
                //Log.e("Tag", "mainData Null ");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        ll_fb.setOnClickListener(this);
        ll_insta.setOnClickListener(this);
        ll_ws.setOnClickListener(this);
        ll_twitter.setOnClickListener(this);
        txt_next.setOnClickListener(this);
        imgv_info.setOnClickListener(this);

        //Twitter Login
        loginButton.setCallback(new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> result) {
                pd = Utility.showProgressDialog(context);
                Log.e("Tag", "loginButton Callback: Success");
                //Log.e("Tag", "loginButton Callback: Success" +result.response.g);
                String oauth_token_secret = result.data.getAuthToken().secret;
                String oauth_token = result.data.getAuthToken().token;
                TwitterSession user = result.data;
                getTwitterUserDetails(user);

            }

            @Override
            public void failure(TwitterException exception) {
                Log.e("Tag", "loginButton Callback: Failure " +
                        exception.getLocalizedMessage());
            }
        });


        //FB Login
        callbackManager = CallbackManager.Factory.create();
        //fb_login_button.setReadPermissions("email", "public_profile", "link");
        fb_login_button.setReadPermissions("email", "public_profile");
        fb_login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("Tag", "FB LOGIN loginResult  : " + loginResult);
                // App code
                //handleFacebookAccessToken(loginResult.getAccessToken());
                Log.e("Tag", "FB LOGIN getAccessToken  : " + loginResult.getAccessToken());
                MyPreference.setPreferenceValue(context, "tokenFB", loginResult.getAccessToken().getUserId());
                String profilePicUrl = "https://graph.facebook.com/" + loginResult.getAccessToken().getUserId() + "/picture?type=large";
                MyPreference.setPreferenceValue(context, "facebookProfileImage", profilePicUrl);
                Log.e("Tag", "FB LOGIN profilePicUrl  : " + profilePicUrl);
                //Utility.dismissProgressDialog(pd);
                //Utility.successDialog("Facebook connected to your profile", context);
                addSocial("facebook", loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                // App code
                Log.e("Tag", "FB LOGIN  onCancel ");
                Utility.errDialog("Facebook Login Is Cancelled By User", context);
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("Tag", "FB LOGIN  exception : " + exception.getMessage());
                Toast.makeText(getContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getTwitterUserDetails(TwitterSession twitterSession) {

        TwitterCore.getInstance().getApiClient(twitterSession).getAccountService().verifyCredentials(true, true, false).enqueue(new Callback<User>() {
            @Override
            public void success(Result<User> userResult) {
                try {
                    User user = userResult.data;
                    String fullname = user.name;
                    long twitterID = user.getId();
                 /*   String userSocialProfile = user.profileImageUrl;
                    String userEmail = user.email;
                    String userFirstNmae = fullname.substring(0, fullname.lastIndexOf(" "));
                    String userLastNmae = fullname.substring(fullname.lastIndexOf(" "));*/
                    String userScreenName = user.screenName;

                   /* Log.e("Tag","twitterID : "+twitterID);
                    Log.e("Tag","userSocialProfile : "+userSocialProfile);
                    Log.e("Tag","userEmail : "+userEmail);
                    Log.e("Tag","userFirstNmae : "+userFirstNmae);
                    Log.e("Tag","userLastNmae : "+userLastNmae);
                    Log.e("Tag","userScreenName : "+userScreenName);*/
                    MyPreference.setPreferenceValue(context, "twitterID", twitterID + "");
                    MyPreference.setPreferenceValue(context, "twitterUserScreenName", userScreenName);
                    addSocial("twitter", twitterID + "");
                    //Utility.dismissProgressDialog(pd);
                    //Utility.successDialog("Twitter connected to your profile", context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(TwitterException e) {
            }
        });
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
        if (view == ll_fb) {
            fb_login_button.performClick();
        } else if (view == ll_insta) {
            customAddInstaWS("instagram");
        } else if (view == ll_twitter) {
            loginButton.performClick();
        } else if (view == ll_ws) {
            customAddInstaWS("whatsapp");
        } else if (view == txt_next) {
            Utility.gotoNext(context, MainActivity.class);
            ((AppCompatActivity) context).finish();
        } else if (view == imgv_info) {
            setPopUpWindow();
            mypopupWindow.showAsDropDown(imgv_info, 0, 10);
        }
    }

    private void setPopUpWindow() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View myView = inflater.inflate(R.layout.popup_view, null);
        TextView txt_msg = myView.findViewById(R.id.txt_msg);
        txt_msg.setText("Sign up with as many social media account as you'd like, choose one now and add more later. This is how you will contact other users!");
        mypopupWindow = new PopupWindow(myView, context.getResources().getDimensionPixelOffset(R.dimen._155sdp), RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        mypopupWindow.setOutsideTouchable(true);
        mypopupWindow.setFocusable(true);
        mypopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        loginButton.onActivityResult(requestCode, resultCode, data);
    }

    //FB LOGIN
    /*private void handleFacebookAccessToken(AccessToken token) {
        Log.e("Tag", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());

        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.e("Tag", "FB signInWithCredential:success");

                    final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    user.getIdToken(true)
                            .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                public void onComplete(@NonNull Task<GetTokenResult> task) {
                                    if (task.isSuccessful()) {
                                        String idToken = task.getResult().getToken();

                                        Utility.dismissProgressDialog(pd);
                                        Utility.successDialog("Facebook connected to your profile", context);
                                    }
                                }
                            });
                } else {
                    // If sign in fails, display a message to the user.
                    Log.e("Tag", "FB signInWithCredential:failure " + task.getException() + "  ---  " + task.getException().getMessage());
                    Utility.errDialog(task.getException().getMessage(), context);
                    //Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                    //updateUI(null);
                }
            }
        });
    }*/

    private void loginTwitterSetup() {
        TwitterAuthConfig mTwitterAuthConfig = new TwitterAuthConfig(getString(R.string.twitter_consumer_key), getString(R.string.twitter_consumer_secret));
        TwitterConfig twitterConfig = new TwitterConfig.Builder(context)
                .twitterAuthConfig(mTwitterAuthConfig)
                .build();
        Twitter.initialize(twitterConfig);

        provider = OAuthProvider.newBuilder("twitter.com");
        Task<AuthResult> pendingResultTask = mAuth.getPendingAuthResult();
        if (pendingResultTask != null) {
            // There's something already here! Finish the sign-in for your user.
            pendingResultTask
                    .addOnSuccessListener(
                            new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                }
                            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                        }
                    });
        } else {
        }
    }

    public void customAddInstaWS(final String type) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_add_insta_ws_dialog);

        final ImageView imgv_icon = dialog.findViewById(R.id.imgv_icon);
        TextView txt_title = dialog.findViewById(R.id.txt_title);
        TextView txt_submit = dialog.findViewById(R.id.txt_submit);
        final EditText edt_id = dialog.findViewById(R.id.edt_id);

        if (type.equals("instagram")) {
            imgv_icon.setImageResource(R.drawable.insta);
            edt_id.setHint("Enter your instagram id");
        } else {
            imgv_icon.setImageResource(R.drawable.ws);
            edt_id.setHint("Enter your mobile number");
        }

        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_id.getText().toString().equals("")) {
                    if (type.equals("instagram")) {
                        Utility.errDialog("Please enter your instagram id", context);
                    } else {
                        Utility.errDialog("Please enter your mobile number", context);
                    }
                } else {
                    pd = Utility.showProgressDialog(context);
                    addSocial(type, edt_id.getText().toString());
                }
            }
        });
        dialog.show();
    }

  /*  private void loginInstagramSetup() {
        authenticationDialog = new AuthenticationDialog(context, this);
        authenticationDialog.setCancelable(true);
        authenticationDialog.show();
    }*/

    private void addSocial(final String key, String value) {
        //pd = Utility.showProgressDialog(context);
        apiServer.addSocial(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    //Log.e("Tag","data changes : "+object);
                    if (object.getString("type").equals("success")) {

                        if (key.contains("facebook")) {
                            ll_fb.setVisibility(View.GONE);
                        } else if (key.contains("twitter")) {
                            ll_twitter.setVisibility(View.GONE);
                        } else if (key.contains("instagram")) {
                            ll_insta.setVisibility(View.GONE);
                        } else if (key.contains("whatsapp")) {
                            ll_ws.setVisibility(View.GONE);
                        }

                        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                        alertDialog2.setTitle("");
                        alertDialog2.setCancelable(false);
                        alertDialog2.setMessage(object.getString("message"));

                        alertDialog2.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                    }
                                });
                        alertDialog2.show();
                    } else {
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissProgressDialog(pd);
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, MyPreference.getPreferenceValue(context, "id").toString(), key, value);
    }

    private void getUserDetail() {
        pd = Utility.showProgressDialog(context);
        apiServer.getUserDetail(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {

                    if (object.getString("type").equals("success")) {
                        JSONObject socialObject = object.getJSONObject("data").getJSONObject("social");

                        Log.e("Tag", "getUserDetail socialObject : " + socialObject);

                        if (!socialObject.getString("facebook").equals("")) {
                            ll_fb.setVisibility(View.GONE);
                        }
                        if (!socialObject.getString("instagram").equals("")) {
                            ll_insta.setVisibility(View.GONE);
                        }
                        if (!socialObject.getString("twitter").equals("")) {
                            ll_twitter.setVisibility(View.GONE);
                        }
                        if (!socialObject.getString("whatsapp").equals("")) {
                            ll_ws.setVisibility(View.GONE);
                        }


                        Utility.dismissProgressDialog(pd);

                        if (ll_fb.getVisibility() == View.GONE && ll_insta.getVisibility() == View.GONE && ll_twitter.getVisibility() == View.GONE && ll_ws.getVisibility() == View.GONE) {
                            txt_all_connect.setVisibility(View.VISIBLE);
                        } else {
                            txt_all_connect.setVisibility(View.GONE);
                        }

                    } else {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, MyPreference.getPreferenceValue(context, "id").toString());
    }

    @Override
    public void onTokenReceived(String auth_token) {
        pd = Utility.showProgressDialog(context);
        apiServer.getInstaToken(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag", "data getInstaToken : " + object);
                    JSONObject jsonData = jsonObject.getJSONObject("data");
                    if (jsonData.has("id")) {

                        Log.e("Tag", "USER_ID : " + jsonData.getString("id"));
                        Log.e("Tag", "USER_NAME : " + jsonData.getString("username"));
                        Log.e("Tag", "PROFILE_PIC : " + jsonData.getString("profile_picture"));

                        /*appPreferences.putString(AppPreferences.USER_ID, jsonData.getString("id"));
                        appPreferences.putString(AppPreferences.USER_NAME, jsonData.getString("username"));
                        appPreferences.putString(AppPreferences.PROFILE_PIC, jsonData.getString("profile_picture"));
*/
                        Utility.successDialog("Instagram connected to your profile", context);
                        Utility.dismissProgressDialog(pd);

                    }
                } catch (Exception e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, getResources().getString(R.string.get_user_info_url) + auth_token);
    }
}
