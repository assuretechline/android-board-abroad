package com.boredabroad.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.boredabroad.AddTripActivity;
import com.boredabroad.ProfileActivity;
import com.boredabroad.R;
import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.global.GPSTracker;
import com.boredabroad.global.GlobalApplication;
import com.boredabroad.global.Utility;
import com.boredabroad.model.UserInfoData;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static androidx.core.content.ContextCompat.checkSelfPermission;

/**
 * Created by home on 12/07/18.
 */

public class MapFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    Context context;

    private static final int PERMISSION_REQUEST_CODE = 1009;
    View view;
    private MapView mapView;
    private GoogleMap mMap;
    TextView txt_name, txt_location, txt_activity;
    CircleImageView imgv_profile;
    LinearLayout ll_user_view, ll_fb, ll_insta, ll_ws, ll_twitter, ll_email;
    ImageView imgv_add, imgv_fb, imgv_twitter, imgv_email;
    View view_dump;
    public static ArrayList<UserInfoData> userInfoDataArrayList = new ArrayList<>();
    int height, selectPosition = 0;
    private ClusterManager<UserInfoData> mClusterManager;
    ProgressDialog pd;
    APIServer apiServer;
    LocationManager locationManager;
    String latitude="51.7320394", longitude="-0.6321988";
    FusedLocationProviderClient fusedLocationClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_map, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();
        if (Utility.checkLocationPermission(context)) {
        } else {
            Utility.requestLocationPermission(context);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
        return view;
    }

    private void initialize() {
        context = getContext();
        apiServer = new APIServer(context);
        Utility.crashLytics(context);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
    }

    private void setView() {
        //mapFragment =(SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        mapView = (MapView) view.findViewById(R.id.map);
        txt_name = view.findViewById(R.id.txt_name);
        txt_location = view.findViewById(R.id.txt_location);
        txt_activity = view.findViewById(R.id.txt_activity);
        imgv_profile = view.findViewById(R.id.imgv_profile);
        ll_user_view = view.findViewById(R.id.ll_user_view);
        ll_fb = view.findViewById(R.id.ll_fb);
        ll_insta = view.findViewById(R.id.ll_insta);
        ll_ws = view.findViewById(R.id.ll_ws);
        ll_twitter = view.findViewById(R.id.ll_twitter);
        ll_email = view.findViewById(R.id.ll_email);
        imgv_fb = view.findViewById(R.id.imgv_fb);
        imgv_twitter = view.findViewById(R.id.imgv_twitter);
        imgv_email = view.findViewById(R.id.imgv_email);
        view_dump = view.findViewById(R.id.view_dump);
        imgv_add = view.findViewById(R.id.imgv_add);
    }

    private void setData() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        displayLocationSettingsRequest();

        pd = Utility.showProgressDialog(context);
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 2030);
        }
    }

    private void setLitionar() {
        ll_fb.setOnClickListener(this);
        ll_email.setOnClickListener(this);
        ll_insta.setOnClickListener(this);
        ll_twitter.setOnClickListener(this);
        ll_ws.setOnClickListener(this);
        imgv_profile.setOnClickListener(this);
        view_dump.setOnClickListener(this);
        imgv_add.setOnClickListener(this);
    }

    private void setColor() {
        imgv_fb.setColorFilter(context.getResources().getColor(R.color.fb));
        imgv_email.setColorFilter(context.getResources().getColor(R.color.email));
        imgv_twitter.setColorFilter(context.getResources().getColor(R.color.twitter));
        imgv_add.setColorFilter(context.getResources().getColor(R.color.white));
    }

    @Override
    public void onClick(View view) {
        if (view == view_dump) {
            Utility.collapse(ll_user_view);
        } else if (view == imgv_profile) {
            Intent i = new Intent(context, ProfileActivity.class);
            i.putExtra("data", selectPosition);
            context.startActivity(i);
            ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
        } else if (view == imgv_add) {
            Utility.gotoNext(context, AddTripActivity.class);
        } else if (view == ll_fb) {
            try {
                String id = new JSONObject(userInfoDataArrayList.get(selectPosition).getSocial()).getJSONObject("social").getString("facebook");
                if (!id.equals("")) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("fb://messaging/" + id)));
                    } catch (Exception e) {

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view == ll_insta) {
            try {
                String id = new JSONObject(userInfoDataArrayList.get(selectPosition).getSocial()).getJSONObject("social").getString("instagram");
                if (!id.equals("")) {
                    Uri uri = Uri.parse("http://instagram.com/_u/" + id);
                    Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                    likeIng.setPackage("com.instagram.android");

                    try {
                        startActivity(likeIng);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://instagram.com/" + id)));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view == ll_twitter) {
            try {
                String id = new JSONObject(userInfoDataArrayList.get(selectPosition).getSocial()).getJSONObject("social").getString("twitter");
                if (!id.equals("")) {
                    Intent intent = null;
                    try {
                        // get the Twitter app if possible
                        getActivity().getPackageManager().getPackageInfo("com.twitter.android", 0);
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=" + id));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        this.startActivity(intent);
                    } catch (Exception e) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + "USERID"));
                        startActivity(intent);
                        Log.e("Tag", "Exception : " + e.getMessage());
                        // no Twitter app, revert to browser
                        //intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/PROFILENAME"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view == ll_ws) {
            try {
                String contact = new JSONObject(userInfoDataArrayList.get(selectPosition).getSocial()).getJSONObject("social").getString("whatsapp");
                Log.e("Tag", "NUmber : " + contact);
                if (!contact.equals("")) {
                    String url = "https://api.whatsapp.com/send?phone=" + contact;
                    try {
                        PackageManager pm = context.getPackageManager();
                        pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    } catch (PackageManager.NameNotFoundException e) {
                        Toast.makeText(context, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view == ll_email) {
            try {
                String id = new JSONObject(userInfoDataArrayList.get(selectPosition).getSocial()).getString("email");
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", id, null));
                intent.putExtra("android.intent.extra.SUBJECT", "");
                startActivity(Intent.createChooser(intent, "Select an email client"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap != null) { //prevent crashing if the map doesn't exist yet (eg. on starting activity)
            mapView.onResume();
            // mMap.clear();

        }
    }

    private void getLocation() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener((Activity) context, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            double lat = location.getLatitude();
                            double longi = location.getLongitude();
                            latitude = String.valueOf(lat);
                            longitude = String.valueOf(longi);
                            //onMapReady(mMap);
                            Log.e("Tag","Data 1 : "+"Your Location: " + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude);
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 2030:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    onMapReady(mMap);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==2230){
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener((Activity) context, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                double lat = location.getLatitude();
                                double longi = location.getLongitude();
                                latitude = String.valueOf(lat);
                                longitude = String.valueOf(longi);
                                Log.e("Tag","Data move : "+"Your Location: " + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude);
                                if (mMap != null) {
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)), 13.0f));
                                }
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("Tag","Failure location : "+e.getMessage());
                }
            });
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        try {
            mMap = googleMap;
            mClusterManager = new ClusterManager<>(context, mMap);
            boolean success = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map));
            mMap.setOnMarkerClickListener(this);
            mMap.setOnCameraIdleListener(mClusterManager);
            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.getUiSettings().setMapToolbarEnabled(false);

            //addMarker();

            if (Utility.isNetworkAvailable(context)) {
                getMapPoints();
            } else {
                Utility.errDialog(context.getString(R.string.network), context);
            }
            //setCustomPin(googleMap);
        } catch (Exception e) {
            //Log.e("Tag", "Map exception : " + e.getMessage());
        }
    }

    private void addMarker() {

        ArrayList<String> imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/1.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/2.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/3.jpg");
        userInfoDataArrayList.add(new UserInfoData("Jack", "London", "Walking", 51.4886073, -0.0665837, imgList));
        mClusterManager.addItem(new UserInfoData("Jack", "London", "Walking", 51.4886073, -0.0665837, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/4.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/5.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/6.jpg");
        userInfoDataArrayList.add(new UserInfoData("Rechard", "London", "Swimming", 51.539664, -0.184690, imgList));
        mClusterManager.addItem(new UserInfoData("Rechard", "London", "Swimming", 51.539664, -0.184690, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/7.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/8.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/9.jpg");
        userInfoDataArrayList.add(new UserInfoData("John", "London", "Walking, Swimming", 51.541259, -0.183724, imgList));
        mClusterManager.addItem(new UserInfoData("John", "London", "Walking, Swimming", 51.541259, -0.183724, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/10.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/11.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/12.jpg");
        userInfoDataArrayList.add(new UserInfoData("Stiwon", "London", "Swimming, Yoga, Cycling", 51.547042, -0.173074, imgList));
        mClusterManager.addItem(new UserInfoData("Stiwon", "London", "Swimming, Yoga, Cycling", 51.547042, -0.173074, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/13.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/14.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/15.jpg");
        userInfoDataArrayList.add(new UserInfoData("Erick", "London", "Walking", 51.540058, -0.174036, imgList));
        mClusterManager.addItem(new UserInfoData("Erick", "London", "Walking", 51.540058, -0.174036, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/16.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/17.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/18.jpg");
        userInfoDataArrayList.add(new UserInfoData("Rick", "London", "Swimming, Yoga, Cycling", 51.544388, -0.182373, imgList));
        mClusterManager.addItem(new UserInfoData("Rick", "London", "Swimming, Yoga, Cycling", 51.544388, -0.182373, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/19.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/20.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/21.jpg");
        userInfoDataArrayList.add(new UserInfoData("Micke", "London", "Bushwalking", 51.546683, -0.171344, imgList));
        mClusterManager.addItem(new UserInfoData("Micke", "London", "Bushwalking", 51.546683, -0.171344, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/22.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/23.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/24.jpg");
        userInfoDataArrayList.add(new UserInfoData("Albert", "London", "Swimming, Yoga, Cycling", 51.545422, -0.167535, imgList));
        mClusterManager.addItem(new UserInfoData("Albert", "London", "Swimming, Yoga, Cycling", 51.545422, -0.167535, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/25.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/26.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/27.jpg");
        userInfoDataArrayList.add(new UserInfoData("Jack", "London", "Bushwalking", 51.541859, -0.179626, imgList));
        mClusterManager.addItem(new UserInfoData("Jack", "London", "Bushwalking", 51.541859, -0.179626, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/28.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/29.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/30.jpg");
        userInfoDataArrayList.add(new UserInfoData("Micke", "London", "Walking", 51.538859, -0.173183, imgList));
        mClusterManager.addItem(new UserInfoData("Micke", "London", "Walking", 51.538859, -0.173183, imgList));

        imgList = new ArrayList<>();
        imgList.add("https://randomuser.me/api/portraits/men/31.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/32.jpg");
        imgList.add("https://randomuser.me/api/portraits/men/33.jpg");
        userInfoDataArrayList.add(new UserInfoData("Johney", "London", "Swimming, Yoga, Cycling", 51.536384, -0.173229, imgList));
        mClusterManager.addItem(new UserInfoData("Johney", "London", "Swimming, Yoga, Cycling", 51.536384, -0.173229, imgList));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.e("Tag", "Data position onMarkerClick " + marker.getTag());
        try {
            int position = (int) (marker.getTag());
            selectPosition = position;
            txt_name.setText(userInfoDataArrayList.get(position).getName());
            txt_activity.setText(userInfoDataArrayList.get(position).getActivity());
            txt_location.setText(userInfoDataArrayList.get(position).getLocation());
            String image;
            try {
                JSONArray jsonArray = new JSONArray(userInfoDataArrayList.get(position).getImage());
                if (jsonArray == null || jsonArray.length() == 0) {
                    image = context.getString(R.string.avtar_image);
                } else {
                    image = jsonArray.getJSONObject(0).getString("thumb");
                }
            } catch (JSONException e) {
                image = context.getString(R.string.avtar_image);
                e.printStackTrace();
            }

            ImageLoader.getInstance().displayImage(image, imgv_profile, Utility.getUserProfileOption());

            if (ll_user_view.getVisibility() == View.GONE) {
                Utility.expand(ll_user_view, height);
            }

            JSONObject jsonObjectSocial = new JSONObject(userInfoDataArrayList.get(position).getSocial()).getJSONObject("social");
            Log.e("Tag","Social : "+jsonObjectSocial);


            ll_fb.setAlpha(1f);
            ll_insta.setAlpha(1f);
            ll_twitter.setAlpha(1f);
            ll_ws.setAlpha(1f);

            if (jsonObjectSocial.getString("facebook").equals("")) {
                ll_fb.setAlpha(0.2f);
            }

            if (jsonObjectSocial.getString("instagram").equals("")) {
                ll_insta.setAlpha(0.2f);
            }

            if (jsonObjectSocial.getString("twitter").equals("")) {
                ll_twitter.setAlpha(0.2f);
            }

            if (jsonObjectSocial.getString("whatsapp").equals("")) {
                ll_ws.setAlpha(0.2f);
            }

        } catch (Exception e) {

        }
        return false;
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap createUserBitmap() {
        Bitmap result = null;
        try {
            result = Bitmap.createBitmap(dp(62), dp(76), Bitmap.Config.ARGB_8888);
            result.eraseColor(Color.TRANSPARENT);
            Canvas canvas = new Canvas(result);

            Paint roundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            RectF bitmapRect = new RectF();
            canvas.save();

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ws);
            //Bitmap bitmap = BitmapFactory.decodeFile(path.toString()); /*generate bitmap here if your image comes from any url*/
            if (bitmap != null) {
                BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                Matrix matrix = new Matrix();
                float scale = dp(52) / (float) bitmap.getWidth();
                matrix.postTranslate(dp(5), dp(5));
                matrix.postScale(scale, scale);
                roundPaint.setShader(shader);
                shader.setLocalMatrix(matrix);
                bitmapRect.set(dp(5), dp(5), dp(52), dp(52));
                canvas.drawRoundRect(bitmapRect, dp(26), dp(26), roundPaint);
            }
            canvas.restore();
            try {
                canvas.setBitmap(null);
            } catch (Exception e) {
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return result;
    }

    public class MarkerClusterRenderer<T extends ClusterItem> extends DefaultClusterRenderer<T> {   // 1
        public MarkerClusterRenderer(Context context, GoogleMap map, ClusterManager<T> clusterManager) {
            super(context, map, clusterManager);
            Log.e("Tag","MarkerClusterRenderer");
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<T> cluster) {
            return cluster.getSize() >= 10;
        }

        @Override
        protected void onBeforeClusterItemRendered(T item, MarkerOptions markerOptions) {
            setCustomPin(mMap, markerOptions);
            super.onBeforeClusterItemRendered(item, markerOptions);
        }

        /* @Override
        protected void onBeforeClusterItemRendered(UserInfoData item, MarkerOptions markerOptions) {
            setCustomPin(mMap,markerOptions);
            super.onBeforeClusterItemRendered(item, markerOptions);
        }*/
    }

    private void setCustomPin(GoogleMap googleMap, MarkerOptions markerOptions) {
        for (int i = 0; i < userInfoDataArrayList.size(); i++) {
            String image;
            try {
                JSONArray jsonArray = new JSONArray(userInfoDataArrayList.get(i).getImage());
                if (jsonArray == null || jsonArray.length() == 0) {
                    image = context.getString(R.string.avtar_image);
                } else {
                    image = jsonArray.getJSONObject(0).getString("thumb");
                }
            } catch (JSONException e) {
                image = context.getString(R.string.avtar_image);
                e.printStackTrace();
            }
            new FetchBitmap(markerOptions, i, image, googleMap, new LatLng(userInfoDataArrayList.get(i).getLat(), userInfoDataArrayList.get(i).getLang())).execute();
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)), 13.0f));
    }

    private class FetchBitmap extends AsyncTask<Void, Void, Bitmap> {
        int position;
        String imageURL;
        LatLng loc;
        GoogleMap myMap;
        MarkerOptions markerOptions;

        public FetchBitmap(MarkerOptions _markerOptions, int positn, String imgURL, GoogleMap mMap, LatLng location) {
            position = positn;
            imageURL = imgURL;
            loc = location;
            myMap = mMap;
            markerOptions = _markerOptions;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Bitmap result = null;
            try {
                result = Bitmap.createBitmap(dp(62), dp(76), Bitmap.Config.ARGB_8888);
                result.eraseColor(Color.TRANSPARENT);
                Canvas canvas = new Canvas(result);
                Paint roundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
                RectF bitmapRect = new RectF();
                canvas.save();

                if (bitmap != null) {
                    BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                    Matrix matrix = new Matrix();
                    float scale = dp(52) / (float) bitmap.getWidth();
                    matrix.postTranslate(dp(5), dp(5));
                    matrix.postScale(scale, scale);
                    roundPaint.setShader(shader);
                    shader.setLocalMatrix(matrix);
                    bitmapRect.set(dp(5), dp(5), dp(52), dp(52));
                    canvas.drawRoundRect(bitmapRect, dp(26), dp(26), roundPaint);


                    Paint drawPaint = new Paint();
                    drawPaint.setColor(Color.parseColor("#fa7d09"));
                    drawPaint.setStrokeWidth(2);
                    drawPaint.setStyle(Paint.Style.STROKE);
                    drawPaint.setStrokeJoin(Paint.Join.ROUND);
                    drawPaint.setStrokeCap(Paint.Cap.ROUND);
                    canvas.drawRoundRect(bitmapRect, dp(26), dp(26), drawPaint);
                }
                canvas.restore();
                try {
                    canvas.setBitmap(null);
                } catch (Exception e) {
                }
            } catch (Exception e) {

            }

            try {

                Marker marker = myMap.addMarker(new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.fromBitmap(result)).anchor(0.5f, 0.907f));
                marker.setTag(position);

            } catch (Exception e) {

            }//Log.e("Tag","Pos : "+position);
            /*MarkerOptions markerOptions = new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.fromBitmap(result)).anchor(0.5f, 0.907f);
            myMap.addMarker(markerOptions);*/
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            return getBitmapFromURL(imageURL);
        }
    }

    public int dp(float value) {
        if (value == 0) {
            return 0;
        }
        return (int) Math.ceil(getResources().getDisplayMetrics().density * value);
    }

    private void getMapPoints() {
        apiServer.getTripList(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    //Log.e("Tag", "data getMapPoints : " + object);
                    /*ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label", object.toString());
                    clipboard.setPrimaryClip(clip);*/
                    if (object.getString("type").equals("success")) {
                        //Utility.errDialog(object.getString("message"), context);

                        JSONArray jsonArrayMain = object.getJSONArray("data");

                        for (int i = 0; i < jsonArrayMain.length(); i++) {
                            //Log.e("Tag","MarkerClusterRenderer * "+i);
                            JSONObject jsonObject = jsonArrayMain.getJSONObject(i);
                            String activity = "";
                            if(jsonObject.getJSONArray("activity")!=null){
                                for (int j = 0; j < jsonObject.getJSONArray("activity").length(); j++) {
                                    JSONArray childArray = jsonObject.getJSONArray("activity").getJSONObject(j).getJSONArray("child");
                                    activity = "";
                                    //Log.e("Tag","MarkerClusterRenderer 3 "+j);
                                    for (int k = 0; k < childArray.length(); k++) {
                                        //Log.e("Tag","MarkerClusterRenderer 4 "+k);
                                        if (k == 0) {
                                            activity += " " + childArray.getJSONObject(k).getString("name") + "";
                                        } else {
                                            activity += ", " + childArray.getJSONObject(k).getString("name") + "";
                                        }
                                    }
                                }
                            }

                            //Log.e("Tag", "Data : " + jsonObject);
                            //Log.e("Tag","MarkerClusterRenderer 2 "+i);
                            try {
                                userInfoDataArrayList.add(new UserInfoData(jsonObject.getJSONObject("user").getString("id"),jsonObject.getJSONObject("user").getString("name"), jsonObject.getString("city") + "," + jsonObject.getString("state"), activity.substring(1), convertToDoble(jsonObject.getString("latitude")), convertToDoble(jsonObject.getString("longitude")), jsonObject.getJSONObject("user").getJSONArray("image").toString(), jsonObject.getJSONObject("user").toString()));
                                mClusterManager.addItem(new UserInfoData(jsonObject.getJSONObject("user").getString("id"),jsonObject.getJSONObject("user").getString("name"), jsonObject.getString("city") + "," + jsonObject.getString("state"), activity.substring(1), convertToDoble(jsonObject.getString("latitude")), convertToDoble(jsonObject.getString("longitude")), jsonObject.getJSONObject("user").getJSONArray("image").toString(), ""));
                            } catch (Exception e) {
                                //Log.e("Tag", "Data Exception : " + e);
                            }
                        }
                        //Log.e("Tag","MarkerClusterRenderer 1 ");
                        mClusterManager.setRenderer(new MarkerClusterRenderer(context, mMap, mClusterManager));
                        mClusterManager.cluster();
                    } else {
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissProgressDialog(pd);
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                    //Log.e("Tag", "Error : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        },latitude,longitude);
    }

    public double convertToDoble(String cordinate) {
        //   Log.e("Tag","Cordinate : "+cordinate.substring(0, cordinate.length() - 1));
        //   return Double.parseDouble(cordinate.substring(0, cordinate.length() - 1));
        return Double.parseDouble(cordinate);
    }

    private void displayLocationSettingsRequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        //fusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e("Tag", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.e("Tag", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(getActivity(), 2230);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e("Tag", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e("Tag","Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

}
