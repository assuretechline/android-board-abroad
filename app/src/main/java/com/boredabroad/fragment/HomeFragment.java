package com.boredabroad.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.boredabroad.R;
import com.boredabroad.adapter.HomeAdapter;
import com.boredabroad.global.Utility;
import com.boredabroad.model.ProductData;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class HomeFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    ArrayList<ProductData> productDataArrayList=new ArrayList<>();
    HomeAdapter homeAdapter;
    ListView lst_product;

    @SuppressLint("ValidFragment")
    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_home, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
    }

    private void setView(View view) {
        lst_product=view.findViewById(R.id.lst_product);
    }

    private void setData() {

        productDataArrayList.add(new ProductData("Pizza Cutter","https://images-na.ssl-images-amazon.com/images/I/41x4pEiADAL.jpg",50,240,0,0,false));
        productDataArrayList.add(new ProductData("Gas Lighter","https://images-na.ssl-images-amazon.com/images/I/51pI4aI8ePL._SL1100_.jpg",35,5,0,0,false));
        productDataArrayList.add(new ProductData("Rice Bowl","https://images-na.ssl-images-amazon.com/images/I/61da6kOd0YL._SL1000_.jpg",30,90,0,0,false));
        productDataArrayList.add(new ProductData("5 Layer Hanger","https://images-na.ssl-images-amazon.com/images/I/61sfRJK1i5L._SL1042_.jpg",55,100,0,0,false));
        productDataArrayList.add(new ProductData("Pizza Cutter","https://images-na.ssl-images-amazon.com/images/I/41x4pEiADAL.jpg",50,120,0,0,false));

        homeAdapter=new HomeAdapter(context,productDataArrayList);
        lst_product.setAdapter(homeAdapter);
    }

    private void setListener() {

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

}