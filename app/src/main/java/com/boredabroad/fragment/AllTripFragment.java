package com.boredabroad.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.boredabroad.R;
import com.boredabroad.adapter.AllTripAdapter;
import com.boredabroad.adapter.HomeAdapter;
import com.boredabroad.api.APIResponse;
import com.boredabroad.api.APIServer;
import com.boredabroad.global.MyPreference;
import com.boredabroad.global.Utility;
import com.boredabroad.model.ProductData;
import com.boredabroad.model.TripListData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class AllTripFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    ArrayList<TripListData> tripListDataArrayList = new ArrayList<>();
    AllTripAdapter allTripAdapter;
    ListView lst_product;
    ProgressDialog pd;
    APIServer apiServer;

    @SuppressLint("ValidFragment")
    public AllTripFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_all_trip, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        apiServer = new APIServer(context);
        Utility.crashLytics(context);
    }

    private void setView(View view) {
        lst_product = view.findViewById(R.id.lst_product);
    }

    private void setData() {
       /* tripListDataArrayList.add(new TripListData("The London Eye","14-09-2020","Walking, Swimming, Yoga, Cycling","London"));
        tripListDataArrayList.add(new TripListData("London Dungeon","10-09-2020","Swimming, Yoga, Cycling","London"));
        tripListDataArrayList.add(new TripListData("Tower of London","04-09-2020","Bushwalking","Briston"));
        tripListDataArrayList.add(new TripListData("The view From The Shard","01-09-2020","Yoga, Cycling","London"));
        tripListDataArrayList.add(new TripListData("The London Eye","24-08-2020","Walking, Swimming, Yoga, Cycling","Belfast"));
        tripListDataArrayList.add(new TripListData("The London Eye","14-08-2020","Walking, Swimming, Yoga, Cycling","London"));
        tripListDataArrayList.add(new TripListData("London Dungeon","08-08-2020","Swimming, Yoga, Cycling","London"));*/


        if (Utility.isNetworkAvailable(context)) {
            getTrip();
        } else {
            Utility.errDialog(context.getString(R.string.network), context);
        }
    }

    private void setListener() {

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

    private void getTrip() {
        pd = Utility.showProgressDialog(context);
        apiServer.getUserDetail(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    //Log.e("Tag","data changes : "+object);
                    if (object.getString("type").equals("success")) {
                        tripListDataArrayList.clear();
                        Utility.dismissProgressDialog(pd);

                        JSONArray jsonArray = object.getJSONObject("data").getJSONArray("trip");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            String activity = "";
                            for (int j = 0; j < jsonObject.getJSONArray("activity").length(); j++) {

                                if (activity.equals("")) {
                                    activity = jsonObject.getJSONArray("activity").getJSONObject(j).getString("name");
                                } else {
                                    activity += ", " + jsonObject.getJSONArray("activity").getJSONObject(j).getString("name");
                                }
                            }
                            tripListDataArrayList.add(new TripListData(
                                    jsonObject.getString("title"),
                                    Utility.setDateShortFormate(jsonObject.getString("date")),
                                    activity,
                                    jsonObject.getString("city") + ", " + jsonObject.getString("state") + ", " + jsonObject.getString("country")));

                        }
                        allTripAdapter = new AllTripAdapter(context, tripListDataArrayList);
                        lst_product.setAdapter(allTripAdapter);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Utility.errDialog(context.getString(R.string.api_failure), context);
            }
        }, MyPreference.getPreferenceValue(context, "id").toString());
    }

}